<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

trait PostImageUploadTrait
{
    /**
     * File upload trait used in controllers to upload files.
     */
    public function saveFiles(Request $request)
    {
        $uploadPath = public_path('/uploads/posts');
        $thumbPath = public_path('/uploads/posts/thumb');

        if (! file_exists($uploadPath)) {
            mkdir($uploadPath, 0775);
            mkdir($thumbPath, 0775);
        }

        $finalRequest = $request;

        foreach ($request->all() as $key => $value) {
            if ($request->hasFile($key)) {

                $filename = time().'-'.$request->file($key)->getClientOriginalName();
                $file = $request->file($key);
                $image = Image::make($file);
                $width = $image->width();
                $height = $image->height();
                
                $image->fit(600, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbPath.'/'.$filename);

                $image->fit(1920, 560, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($uploadPath.'/'.$filename);


                $finalRequest = new Request(array_merge($finalRequest->all(), [$key => $filename]));
           
            }
        }

        return $finalRequest;
    }
}
