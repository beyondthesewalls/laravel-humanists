@extends('layouts.front')
@section('content')

<div class="flex flex-wrap overflow-hidden md:-mx-5 lg:-mx-5 xl:-mx-5">

  <div class="w-full overflow-hidden md:my-1 md:px-1 md:w-1/3 lg:my-2 lg:px-2 lg:w-1/3 xl:my-2 xl:px-2 xl:w-1/3">
    <img class="object-cover object-center mr-10 " alt="hero" src="/assets/images/mail.png">
  </div>

  <div class="w-full overflow-hidden md:my-20 md:px-1 md:w-2/3 lg:my-10 lg:px-5 lg:w-2/3 xl:my-10 xl:px-5 xl:w-2/3 ">



    @if ($sent == 1)
    <div>
        <h2 class="text-4xl font-bold tracking-tight">Signup</h2>
        <div class="mt-5 bg-custom-100 p-3 rounded">
            <h3 class="text-xl font-bold tracking-tight ">Thanks for signing up! </h3>
            @if ($hasmember != 1)
            <div class="mt-2 mb-5">Please check your inbox to verify your email address. (It may be in your Spam folder.)</div>
            @endif
        </div>
    </div>
    @else
    <x-form.open method="POST" action="{{route('signup.store')}}" id="form">
        @method('POST')

    
        <h2 class="text-4xl font-bold tracking-tight">Signup</h2>

        <p class="mt-5">Use this form to signup for either our newsletter or membership (or both!)</p>

        <div class="flex flex-wrap overflow-hidden sm:-mx-1 md:-mx-1 lg:-mx-1 xl:-mx-1 mt-5">

          <div class="w-full overflow-hidden sm:my-1 sm:px-1 sm:w-1/2 md:my-1 md:px-1 md:w-1/2 lg:my-1 lg:px-1 lg:w-1/2 xl:my-1 xl:px-1 xl:w-1/2">
            <label for="FNAME" class="block font-medium text-gray-500">First Name <span class="text-red-500">*</span></label>
            <input type="text" name="FNAME" id="mce-FNAME" value="" class="transition duration-200 border border-gray-300 bg-white text-gray-900 appearance-none sm:text-md sm:leading-5 rounded-sm py-2 px-2 focus:border-custom-400 focus:outline-none mb-4 mt-1 block w-full" required>          </div>

          <div class="w-full overflow-hidden sm:my-1 sm:px-1 sm:w-1/2 md:my-1 md:px-1 md:w-1/2 lg:my-1 lg:px-1 lg:w-1/2 xl:my-1 xl:px-1 xl:w-1/2">
            <label for="LNAME" class="block font-medium text-gray-500">Last Name <span class="text-red-500">*</span></label>
            <input type="text" name="LNAME" id="mce-LNAME" value="" class="transition duration-200 border border-gray-300 bg-white text-gray-900 appearance-none sm:text-md sm:leading-5 rounded-sm py-2 px-2 focus:border-custom-400 focus:outline-none mb-4 mt-1 block w-full" required>
          </div>

        </div>


        <div class="mb-5">
            <label for="EMAIL" class="block font-medium text-gray-500">Email <span class="text-red-500">*</span></label>
            <input type="email" value="" name="EMAIL" id="mce-EMAIL" class="transition duration-200 border border-gray-300 bg-white text-gray-900 appearance-none sm:text-md sm:leading-5 rounded-sm py-2 px-2 focus:border-custom-400 focus:outline-none mb-4 mt-1 block w-full" required>
        </div>

     

        <div class="border border-gray-300 rounded p-4 mt-5">
            <h3 class="text-xl font-bold tracking-tight ">Newsletter</h3>
            <p class="mt-5">Receive regular updates, usually monthly, on our upcoming activities.</p>
            <div class="mt-3">
                <label for="active" class="inline-flex items-center">
                    <input type="checkbox" value="410d53c68d" name="interest1" id="interest1" class="form-checkbox h-5 w-5 text-custom-600 transition duration-150 ease-in-out mt-1 text-green-600">
                    <span class="ml-2 font-medium font-bold text-gray-700">Newsletter</span>
                </label>
            </div>
        </div>

        <div class="border border-gray-300 rounded p-4  mt-5">
            <h3 class="text-xl font-bold tracking-tight ">Membership </h3>
            <p class="mt-5">If you would like to be counted as a member of our group then please tick below.  There is no obligation to join and you will still be most welcome at any of our events.
                <BR><BR>
                The benefits of becoming a member are that we will keep you informed of any decisions that are made about how the group operates.  Membership will also allow you to participate in our AGM and influence how the group is run. You will receive one or two emails per year.
                <div class="my-3">
                    <label for="active" class="inline-flex items-center">
                        <input type="checkbox" value="c9fedc5641" name="interest2" id="interest2" class="form-checkbox h-5 w-5 text-custom-600 transition duration-150 ease-in-out mt-1 text-green-600">
                        <span class="ml-2 font-bold font-medium text-gray-700">I wish to be a member of Central London Humanists (no cost)</span>
                    </label>
                </div>
                <BR>
                <p class="">If you would like to vote at our AGM or become a committee member you will also need to be a member of <a href="https://humanism.org.uk/join/" target="_blank" class="text-custom-500">Humanists UK</a>.</p>
                <div class="my-3">
                    <label for="active" class="inline-flex items-center">
                        <input type="checkbox" value="c90bc770f9" name="interest3" id="interest3" class="form-checkbox h-5 w-5 text-custom-600 transition duration-150 ease-in-out mt-1 text-green-600">
                        <span class="ml-2 font-medium font-bold text-gray-700">I'm a member of Humanists UK</span>
                    </label>
                </div>
            </p>
        </div>

        <div class="border border-gray-300 rounded p-4 mt-5">
            <div class="content__gdpr">
                <p>You can unsubscribe or change your choices at any time by clicking the link in the footer of our emails.</p><BR>
            </div>
            <div class="text-sm">
                <p>We use Mailchimp as our marketing platform. By clicking below to subscribe, you acknowledge that your information will be transferred to Mailchimp for processing. Learn more about Mailchimp's privacy practices <a href="https://mailchimp.com/legal/" target="_blank" class="text-custom-500">here.</a></p>
            </div>

            <input type="submit" value="Subscribe" name="subscribe" id="subscribe" class="mt-5 bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded">


        </div>

    </x-form.open> 
    @endif
  </div>

</div>

@endsection

