
<x-form.input type="text" name='subject' label='Subject' class="block w-full ">{{ $newsletterdraft->subject }}</x-form.input>

<x-form.input type="text" name='from_email' label='From Email' class="block w-full ">{{ $newsletterdraft->from_email }}</x-form.input>

<x-form.select name="from_name" label="Author" :options="$users" class="block w-full" :setvalue="$newsletterdraft->from_name" />

<div class="mt-4">
    <label class="block">
                <label for="list_id" class="block text-sm font-medium text-gray-400">List</label>
        
        <select name="list_id" id="list_id" class="transition duration-200 border border-gray-300 bg-white text-gray-900 appearance-none sm:text-md sm:leading-5 rounded-sm py-2 px-2 focus:border-custom-400 focus:outline-none mb-4 mt-1 block w-full" label="Author" options="">
           @foreach($interests as $col )
                <option value="{{ $col['id'] }}">{{ $col['name'] }}</option>
            @endforeach
                    </select>
    </label>

</div>

<input type="hidden" id="content" name="content" />
<input type="hidden" id="test_email" name="test_email" />

