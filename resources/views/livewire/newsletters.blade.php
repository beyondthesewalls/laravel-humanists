<div wire:poll.60s>
    <h1 class="text-2xl font-medium text-gray-700">Newsletters</h1>

    <x-flash /> 

    <div class="py-4 space-y-4">
        <!-- Top Bar -->
        <div class="flex justify-between">
            <div class="w-2/4 flex space-x-4 ">
                 <x-input.search leading-icon="true" reset-filters="clearSearch()" wire:model="search" placeholder="Search..." />
            </div>

            <div class="space-x-2 flex items-center">
                <x-input.group borderless paddingless for="filter" label="Per Page">
                    <x-input.select wire:model="filter" id="filter">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

                <a href="{{ route('admin.newsletters.create') }}"><x-button.primary><x-icon.plus/> New</x-button.primary></a>
            </div>
        </div>

             <!-- Transactions Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading sortable multi-column wire:click="sortBy('settings.title')" :direction="$sorts['settings.title'] ?? null" class="w-full">Title</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('create_time')" :direction="$sorts['create_time'] ?? null">Created</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('recipients.recipient_count')" :direction="$sorts['recipients.recipient_count'] ?? null">Count</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('report_summary.opens')" :direction="$sorts['report_summary.opens'] ?? null">Opens</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('report_summary.click_rate')" :direction="$sorts['report_summary.click_rate'] ?? null">Clicks</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('status')" :direction="$sorts['status'] ?? null">Status</x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @forelse ($pages as $page)
                    <x-table.row wire:loading.class.delay="opacity-50" wire:key="page-{{ $page['id'] }}">                        
                        <x-table.cell>
                            <span href="#" class="inline-flex space-x-2 truncate text-sm leading-5">
                                <x-icon.document class="text-cool-gray-400"/>

                                <p class="text-cool-gray-600 truncate">
                                    {{ $page['settings']['title'] ?? ''}}
                                </p>
                            </span>
                        </x-table.cell>

                        <x-table.cell>
                            {{ date('d/m/Y', strtotime($page['create_time']))}}                          
                        </x-table.cell>

                         <x-table.cell>
                          @if ($page['status'] == 'sent')
                            {{ $page['recipients']['recipient_count'] ?? '' }}
                         @endif
                        </x-table.cell>

                        <x-table.cell>
                             {{ $page['report_summary']['opens'] ?? ''  }}                        
                        </x-table.cell>

                        <x-table.cell>
                             {{ $page['report_summary']['click_rate'] ?? '' }}                        
                        </x-table.cell>

                        <x-table.cell>
                             @if ($page['status'] == 'sent')
                                <span class="rounded-lg py-1 px-2 bg-green-50">Sent</span>
                            @elseif ($page['status'] == 'sending')
                              <span class="rounded-lg py-1 px-2 bg-custom-50">Sending</span>
                            @else
                                <span class="rounded-lg py-1 px-2  bg-gray-100">Draft</span>
                            @endif
                        </x-table.cell>

                    

                        <x-table.cell class="flex justify-end">                           

                            @if ($page['status'] != 'sent')
                            <a href="{{ route('admin.newsletters.edit', $page['id']) }}">
                                <button class="bg-blue-500 text-white active:bg-blue-600 text-xs px-2 py-1 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button">Edit</button>
                            </a>

                            <button type="button" class="bg-indigo-500 text-white active:bg-indigo-600 text-xs px-2 py-1 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" wire:click="test('{{$page['id']}}')">
                                Test
                            </button>

                            <button type="button" class="bg-green-500 text-white active:bg-green-600 text-xs px-2 py-1 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" wire:click="send('{{$page['id']}}')">
                                Send
                            </button>                          

                            <button type="button" class="bg-red-500 text-white active:bg-red-600 text-xs px-2 py-1 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" wire:click="delete('{{$page['id']}}')">
                                Delete
                            </button>
                            
                            @endif
                        </x-table.cell>
                    </x-table.row>
                    @empty
                    <x-table.row>
                        <x-table.cell colspan="7">
                            <div class="flex justify-center items-center space-x-2">
                                <x-icon.inbox class="h-8 w-8 text-cool-gray-400" />
                                <span class="font-medium py-8 text-cool-gray-400 text-xl">No newsletters found...</span>
                            </div>
                        </x-table.cell>
                    </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>
            {{ $pages->links('vendor.pagination.livewire-tailwind') }}
            <div>
                
            </div>
        </div>
    </div>

    <!-- Delete Transactions Modal -->
    <form wire:submit.prevent="deleteSingle">
        <x-modal.confirmation wire:model.defer="showDeleteSingleModal">
            <x-slot name="title">Delete Newsletter?</x-slot>

            <x-slot name="content">
                <div class="py-8 text-cool-gray-700">Are you sure? This action is irreversible.</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteSingleModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Delete</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>

    <!-- Delete Transactions Modal -->
    <form wire:submit.prevent="sendTest">
        <x-modal.confirmation wire:model.defer="showTestModal">
            <x-slot name="title">Test Newsletter?</x-slot>

            <x-slot name="content">
                <div>
                        <label for="testemail" class="block text-sm font-medium text-gray-400">Enter Test Email</label>
                        <input type="text" name="testemail" id="testemail" value="" class="transition duration-200 border border-gray-300 bg-white text-gray-900 appearance-none sm:text-md sm:leading-5 rounded-sm py-2 px-2 focus:border-custom-400 focus:outline-none mb-4 mt-1 block w-full" wire:model="testemail" required="required">
                </div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showTestModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Send</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>

    <!-- Delete Transactions Modal -->
    <form wire:submit.prevent="sendEmail">
        <x-modal.confirmation wire:model.defer="showSendModal">
            <x-slot name="title">Send Newsletter?</x-slot>

            <x-slot name="content">
                <div class="py-8 text-cool-gray-700" >This will send an email to everyone on the list. <BR> Recommended to send a test email first.</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showSendModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Send</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>

</div>
