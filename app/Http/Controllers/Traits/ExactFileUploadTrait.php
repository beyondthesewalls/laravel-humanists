<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

trait ExactFileUploadTrait
{
    /**
     * File upload trait used in controllers to upload files.
     */
    public function saveFiles(Request $request)
    {
        if ($request->location != '') {
            $uploadPath = public_path($request->location.'/');
            $thumbPath = public_path($request->location.'/thumb');
        } else {
            $uploadPath = public_path('/uploads');
            $thumbPath = public_path('/uploads/thumb');
        }

        if (! file_exists($uploadPath)) {
            mkdir($uploadPath, 0775);
            mkdir($thumbPath, 0775);
        }

        $finalRequest = $request;

        foreach ($request->all() as $key => $value) {
            if ($request->hasFile($key)) {
                if ($request->has($key.'_max_width') && $request->has($key.'_max_height')) {
                    // Check file width
                    $filename = rand(1111111111,9999999999).'-'.$request->file($key)->getClientOriginalName();
                    $file = $request->file($key);
                    $image = Image::make($file);
                    if (! file_exists($thumbPath)) {
                        mkdir($thumbPath, 0775, true);
                    }
                    Image::make($file)->resize(150, 150)->save($thumbPath.'/'.$filename);
                    $width = $image->width();
                    $height = $image->height();

                    $image->fit($request->{$key.'_max_width'}, $request->{$key.'_max_height'});

                    $image->save($uploadPath.'/'.$filename);
                    
                    $finalRequest = new Request(array_merge($finalRequest->all(), [$key => $filename]));
                } else {
                    $filename = time().'-'.$request->file($key)->getClientOriginalName();
                    $request->file($key)->move($uploadPath, $filename);
                    $finalRequest = new Request(array_merge($finalRequest->all(), [$key => $filename]));
                }
            }
        }

        return $finalRequest;
    }
}
