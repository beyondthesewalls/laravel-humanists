@props([
    'type' => 'mainItem',
    'colors' => [
        'mainItem' => 'hover:bg-custom-400 text-sm cursor-pointer px-2',
        'resItem' => 'hover:bg-custom-500 mb-2 items-center justify-center cursor-pointer',
    ]
])

<div {{ $attributes->merge(['class' => "{$colors[$type]} text-gray-300 dark:text-gray-200 hover:text-white flex rounded-lg my-2 py-2"]) }}>
    {{ $slot }}
</div>
