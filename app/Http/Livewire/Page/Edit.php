<?php

namespace App\Http\Livewire\Page;

use App\Models\Page;
use Livewire\Component;

class Edit extends Component
{
    public Page $page;

    public array $listsForFields = [];

    public function mount(Page $page)
    {
        $this->page = $page;
    }

    public function render()
    {
        return view('livewire.pages.edit');
    }

    public function submit()
    {
        $this->validate();

        $this->page->save();

        // return redirect()->route('admin.pages.index');
    }

    protected function rules(): array
    {
        return [
            'page.menu_title' => [
                'string',
                'required',
            ],
            'page.title' => [
                'string',
                'required',
            ],
            'page.content' => [
                'string',
                'nullable',
            ],
            'page.image_id' => [
                'integer',
                'exists:images,id',
                'nullable',
            ],
            'page.menu' => [
                'boolean',
            ],
            'page.order' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'page.active' => [
                'boolean',
            ],
        ];
    }


}
