<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use App\Http\Controllers\Traits\PostImageUploadTrait;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    use PostImageUploadTrait;

    public function index()
    {
        abort_if(Gate::denies('post_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.posts.index');
    }

    public function create()
    {
        // abort_if(Gate::denies('post_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $post = new Post(['user_id' => 2, 'title' => 'hello']);
        $users = User::orderBy('name')->get()->pluck('full_name', 'id');
        $categories = Category::orderBy('title')->get()->pluck('title', 'id');

        return view('admin.posts.create', compact('post', 'users', 'categories'));

    }

    public function store(Request $request)
    {
        // abort_if(Gate::denies('post_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->request->add(['location' => '/uploads/posts']);
        $request = $this->saveFiles($request);
        $post = Post::create($request->all());

        // alert()->success('Page created', 'Success')->autoClose(3000);

        return redirect()->route('admin.posts.index');


    }

    public function edit(Post $post)
    {
        // abort_if(Gate::denies('post_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $users = User::orderBy('name')->get()->pluck('full_name', 'id');
        $categories = Category::orderBy('title')->get()->pluck('title', 'id');
        return view('admin.posts.edit', compact('post', 'users', 'categories'));
    }

    public function update(Request $request, Post $post)
    {
        $request = $this->saveFiles($request);
        $post->update($request->all());

        return redirect()->route('admin.posts.index');
    }

    public function storeMedia(Request $request)
    {
        abort_if(Gate::none(['post_create', 'post_edit']), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->has('size')) {
            $this->validate($request, [
                'file' => 'max:' . $request->input('size') * 1024,
            ]);
        }
        if (request()->has('max_width') || request()->has('max_height')) {
            $this->validate(request(), [
                'file' => sprintf(
                'image|dimensions:max_width=%s,max_height=%s',
                request()->input('max_width', 100000),
                request()->input('max_height', 100000)
                ),
            ]);
        }

        $model                     = new Post();
        $model->id                 = $request->input('model_id', 0);
        $model->exists             = true;
        $media                     = $model->addMediaFromRequest('file')->toMediaCollection($request->input('collection_name'));
        $media->wasRecentlyCreated = true;

        return response()->json(compact('media'), Response::HTTP_CREATED);
    }
}
