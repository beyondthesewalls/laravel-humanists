<?php

namespace App\Http\Livewire\Comment;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Livewire\Component;

class Create extends Component
{
    public Comment $comment;

    public array $listsForFields = [];

    public function mount(Comment $comment)
    {
        $this->comment = $comment;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.comment.create');
    }

    public function submit()
    {
        $this->validate();

        $this->comment->save();

        return redirect()->route('admin.comments.index');
    }

    protected function rules(): array
    {
        return [
            'comment.post_id' => [
                'integer',
                'exists:posts,id',
                'required',
            ],
            'comment.user_id' => [
                'integer',
                'exists:users,id',
                'required',
            ],
            'comment.title' => [
                'string',
                'required',
            ],
            'comment.content' => [
                'string',
                'required',
            ],
            'comment.active' => [
                'boolean',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['post'] = Post::pluck('title', 'id');
        $this->listsForFields['user'] = User::pluck('first_name', 'id');
    }
}
