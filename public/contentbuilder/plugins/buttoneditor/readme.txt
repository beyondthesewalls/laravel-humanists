﻿Preview Plugin

To install the plugin, modify config file (newsletterbuilder\config.js) as follow:

	_cb.settings.plugins = ['buttoneditor'];

When you click a button, there will be an 'Edit' icon displayed.