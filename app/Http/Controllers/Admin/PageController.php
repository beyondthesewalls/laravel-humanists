<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\MeetupService;
use Carbon\Carbon;


class PageController extends Controller
{

    public function index()
    {
        // abort_if(Gate::denies('page_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pages.index');
    }

    public function create()
    {
        // abort_if(Gate::denies('page_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $page = new Page();
        
        return view('admin.pages.create', compact('page'));
    }

    public function store(Request $request)
    {
        // abort_if(Gate::denies('page_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $page = Page::create($request->all());

        // alert()->success('Page created', 'Success')->autoClose(3000);

        return redirect()->route('admin.pages.index');
    }

    public function edit(Page $page)
    {
        // abort_if(Gate::denies('page_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pages.edit', compact('page'));
    }

    public function update(Request $request, Page $page)
    {
        // abort_if(Gate::denies('page_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
       
        // request()->validate(Page::$rules);

        $page->update($request->all());

        // alert()->success('Page Updated', 'Success')->autoClose(3000);

        return redirect()->route('admin.pages.index');
    }

    public function snippets(MeetupService $meetupservice)
    {
        abort_if(Gate::denies('page_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $json = $meetupservice->events();

        $now = Carbon::now();
        $date = $now->format('Y').'-'.$now->addMonth()->format('m').'-01';
        $date1 = $now->format('Y').'-'.$now->addMonth(1)->format('m').'-01';

        $thismonth = $json->where('local_date', '<', $date);
        $nextmonth = $json->where('local_date', '>', $date)->where('local_date', '<', $date1);


        $events = json_decode($json);
        $events = array_slice($events, 0, 6);

        $thismonth = json_decode($thismonth);
        $nextmonth = json_decode($nextmonth);


        return view('admin.pages.snippets', compact('events', 'thismonth', 'nextmonth'));
    }

// /2022-05-04

}
