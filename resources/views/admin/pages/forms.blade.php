<x-form.input type="text" name='title' label='Title' class="block w-full ">{{ $page->title }}</x-form.input>
<x-form.input type="text" name='menu_title' label='Menu Title' class="block w-full">{{ $page->menu_title }}</x-form.input>
<x-form.input type="number" name='order' label='Order' class="block">{{ $page->order }}</x-form.input>
<x-form.checkbox name='active' label='Active' class="text-green-600">{{ $page->active }}</x-form.checkbox>
<x-form.checkbox name='menu' label='Show on Menu' class="">{{ $page->menu }}</x-form.checkbox>
<input type="hidden" name="content" id="content">