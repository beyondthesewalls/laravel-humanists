<?php

namespace App\Http\Livewire\Gallery;

use App\Models\Gallery;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Create extends Component
{
    public Gallery $gallery;

    public array $mediaToRemove = [];

    public array $mediaCollections = [];

    public function mount(Gallery $gallery)
    {
        $this->gallery = $gallery;
    }

    public function render()
    {
        return view('livewire.gallery.create');
    }

    public function submit()
    {
        $this->validate();

        $this->gallery->save();
        $this->syncMedia();

        return redirect()->route('admin.galleries.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    protected function rules(): array
    {
        return [
            'gallery.title' => [
                'string',
                'required',
            ],
            'mediaCollections.gallery_images' => [
                'array',
                'nullable',
            ],
            'mediaCollections.gallery_images.*.id' => [
                'integer',
                'exists:media,id',
            ],
        ];
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->gallery->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
