<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Page;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\MeetupService;
use DB;


// use Spatie\Searchable\Search;

class ContentController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = Page::where('id', 1)->first();
        $active = '';

        $pages = Page::all();

        return view('front.pages.page', compact('content', 'active'));
        // return view('front.pages.test', compact('content', 'active'));
    }

    public function signup()
    {

        return view('front.pages.signup');
        // return view('front.pages.test', compact('content', 'active'));
    }

    public function page($page)
    {

        // $frontmenu = Page::where('parent_id', null)->get();
        $content = Page::where('slug', $page)->first();
        $active = $page;

        if ($content->parent_id != null) {
            $active = $content->parent->slug;
        }

        return view('front.pages.page', compact('content', 'active'));
    }

    public function api_meetup(MeetupService $meetupservice)
    {
        $json = $meetupservice->events();

        $events = json_decode($meetupservice->events());

        $events = array_slice($events, 0, 6);

        return $events;
        // return view('front.pages.test', compact('content', 'active'));
    }

    public function api_posts()
    {
    
        $posts = Post::with('user')
            ->select('id', 'title', 'slug', 'user_id', 'image', 'readtime')
            ->addSelect(DB::raw("DATE_FORMAT(published, '%D %M %Y') AS display_date"))
            ->orderBy('published', 'DESC')->where('published', '!=', '')->where('active', '1')
            ->take(3)
            ->get();

        return $posts;
       
    }

    public function posts()
    {
        $featured = Post::where('featured', 1)->first();

        $posts = Post::with('user')
            ->select('id', 'title', 'slug', 'summary', 'user_id', 'image', 'readtime', 'featured','published')
            ->addSelect(DB::raw("DATE_FORMAT(published, '%D %M %Y') AS display_date"))
            ->where('featured', '!=', 1)
            ->orderBy('published', 'DESC')->where('published', '!=', '')->where('active', '1')
            ->get();

        $newposts = Post::with('user')
            ->select('id', 'title', 'slug', 'summary', 'user_id', 'image', 'readtime', 'featured','published')
            ->addSelect(DB::raw("DATE_FORMAT(published, '%D %M %Y') AS display_date"))
            ->orderBy('published', 'DESC')->where('published', '!=', '')->where('active', '1')
            ->take(5)
            ->get();

        $categories = Category::orderBy('title', 'asc')->whereHas('posts')->get();

        $active = 'post';

        return view('front.posts.index', compact('featured', 'posts', 'categories', 'active', 'newposts'));
        // return view('front.pages.test', compact('content', 'active'));
    }

    public function category($id)
    {
        $posts = Post::
            whereHas('category', function ($query) use ($id) {
                return $query->where('slug', $id);
            })
            ->select('id', 'title', 'slug', 'summary', 'user_id', 'image', 'readtime', 'published', 'category_id')
            ->addSelect(DB::raw("DATE_FORMAT(published, '%D %M %Y') AS display_date"))
            ->orderBy('published', 'DESC')->where('published', '!=', '')->where('active', '1')
            ->with('user', 'category')
            ->get();

        $category = Category::where('slug', $id)->first();

        $active = 'post';

        return view('front.posts.category', compact('posts', 'category', 'active'));
        // return view('front.pages.test', compact('content', 'active'));
    }


    public function post($id)
    {
        $post = Post::
           where('slug', $id)
            ->select('id', 'title', 'slug', 'summary', 'content', 'user_id', 'image', 'readtime', 'published', 'category_id')
            ->addSelect(DB::raw("DATE_FORMAT(published, '%D %M %Y') AS display_date"))
            ->orderBy('published', 'DESC')->where('published', '!=', '')->where('active', '1')
            ->with('user', 'category')
            ->first();

        $active = 'post';

        return view('front.posts.post', compact('post', 'active'));
        // return view('front.pages.test', compact('content', 'active'));
    }
    
}
