<div>
    <h1 class="text-2xl font-medium text-gray-700">Posts</h1>

    <div class="py-4 space-y-4">
        <!-- Top Bar -->
        <div class="flex justify-between">
            <div class="w-2/4 flex space-x-4 ">
                 <x-input.search leading-icon="true" reset-filters="resetFilters()" wire:model="filters.search" placeholder="Search Posts..." />
            </div>

            <div class="space-x-2 flex items-center">
                <x-input.group borderless paddingless for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

                <a href="{{ route('admin.posts.create') }}"><x-button.primary><x-icon.plus/> New</x-button.primary></a>
            </div>
        </div>

             <!-- Transactions Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading sortable multi-column wire:click="sortBy('title')" :direction="$sorts['title'] ?? null" class="w-full">Title</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('order')" :direction="$sorts['order'] ?? null">Order</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('status')" :direction="$sorts['status'] ?? null">Status</x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @forelse ($posts as $post)
                    <x-table.row wire:loading.class.delay="opacity-50" wire:key="post-{{ $post->id }}">                        
                        <x-table.cell>
                            <span href="#" class="inline-flex space-x-2 truncate text-sm leading-5">
                                <x-icon.document class="text-cool-gray-400"/>

                                <p class="text-cool-gray-600 truncate">
                                    {{ $post->title }}
                                </p>
                            </span>
                        </x-table.cell>

                        <x-table.cell>
                          
                                <span class="text-cool-gray-900 font-medium">{{ $post->order }} </span>
                         
                        </x-table.cell>

                        <x-table.cell>
                             @if ($post->active == 1)
                                <span class="rounded-lg py-1 px-2 bg-green-50">Active</span>
                            @endif
                        </x-table.cell>

                    

                        <x-table.cell class="flex justify-end">
                            <a href="{{ route('admin.posts.edit', $post) }}">
                                <x-icon.edit class="h-8 w-8 text-green-600 flex"/> 
                            </a>
                            <x-button.link wire:click="delete({{ $post->id }})">
                                 <x-icon.trash class="h-8 w-8 text-red-600 flex"/> 
                            </x-button.link>
                        </x-table.cell>
                    </x-table.row>
                    @empty
                    <x-table.row>
                        <x-table.cell colspan="6">
                            <div class="flex justify-center items-center space-x-2">
                                <x-icon.inbox class="h-8 w-8 text-cool-gray-400" />
                                <span class="font-medium py-8 text-cool-gray-400 text-xl">No posts found...</span>
                            </div>
                        </x-table.cell>
                    </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $posts->links() }}
            </div>
        </div>
    </div>

    <!-- Delete Transactions Modal -->
    <form wire:submit.prevent="deleteSelected">
        <x-modal.confirmation wire:model.defer="showDeleteModal">
            <x-slot name="title">Delete Posts</x-slot>

            <x-slot name="content">
                <div class="py-8 text-cool-gray-700">Are you sure you? This action is irreversible.</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Delete</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>

    <!-- Delete Transactions Modal -->
    <form wire:submit.prevent="deleteSingle">
        <x-modal.confirmation wire:model.defer="showDeleteSingleModal">
            <x-slot name="title">Delete Post</x-slot>

            <x-slot name="content">
                <div class="py-8 text-cool-gray-700">Are you sure you? This action is irreversible.</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteSingleModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Delete</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>
</div>
