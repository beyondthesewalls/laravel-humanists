
<x-form.input type="text" name='title' label='Title' class="block w-full ">{{ $post->title }}</x-form.input>

<x-form.select name="user_id" label="Author" :options="$users" class="block w-full" :setvalue="$post->user_id" />

<x-form.select name="category_id" label="Category" :options="$categories" class="block w-full" :setvalue="$post->category_id" />

<x-form.input type="text" name='readtime' label='Read time in minutes' class="block w-full ">{{ $post->readtime }}</x-form.input>

<x-form.input type="text" name='published' label='Published Date' class="flatpickr flatpickr-input active block w-full " placeholder="Select Date.." data-id="datetime" readonly="readonly" >{{ $post->published }}</x-form.input>

<div class="shadow-lg mb-5 ">

	@if ($post->image)
	    <a href="{{ asset('/uploads/posts/'.$post->image) }}" target="_blank">
	    	<img src="{{ asset('/uploads/posts/thumb/'.$post->image) }}" style="max-width: 300px" class="rounded-tl-md rounded-tr-md ">
	    </a>
	@else
		<img src="{{ asset('/uploads/posts/main_blank.png') }}" style="max-width: 300px" class="rounded-tl-md rounded-tr-md bg-gray-200">
	@endif
	<div class="flex w-full items-center justify-center bg-grey-lighter ">
	    <label class="w-full flex flex-col items-center px-4 pb-2 bg-white rounded-bl-md rounded-br-md cursor-pointer hover:bg-custom-600 hover:text-white text-sm font-medium text-gray-400">
	        <span class="mt-2 ">Upload header image</span>
	        <input type='file' name="image" class="hidden" />
	    </label>
	</div>

</div>

<!-- <input type="file" name="image" /> -->




<x-form.checkbox name='featured' label='Featured' class="">{{ $post->featured }}</x-form.checkbox>
<x-form.checkbox name='active' label='Active' class="text-green-600">{{ $post->active }}</x-form.checkbox>


<button type="submit" class="mt-3 py-2 px-4 border rounded-md text-sm leading-5 font-medium focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition duration-150 ease-in-out text-white bg-custom-600 hover:bg-custom-500 active:bg-custom-700 border-custom-600"> Save</button>