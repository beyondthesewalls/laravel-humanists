<aside class="w-full md:w-64 bg-gray-800 md:min-h-screen" x-data="{ isOpen: false }">
    <div class="flex items-center justify-between bg-gray-900 p-4 h-16">


        <a href="#" class="flex items-center">
      

            <span class="text-gray-300 text-xl font-semibold mx-2">Dashboard</span>
        </a>
        <div class="flex md:hidden">
            <button type="button" @click="isOpen = !isOpen"
                    class="text-gray-300 hover:text-gray-500 focus:outline-none focus:text-gray-500">
                <svg class="fill-current w-8" fill="none" stroke-linecap="round" stroke-linejoin="round"
                     stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                    <path d="M4 6h16M4 12h16M4 18h16"></path>
                </svg>
            </button>
        </div>
    </div>
    <div class="px-2 py-6 md:block" :class="isOpen? 'block': 'hidden'" >

<!--         <div x-data="{ open: false }">
            <a href="#" @click="open=true" class="group hover:bg-green-400 text-gray-600 dark:text-gray-200 text-sm hover:text-white px-2 py-2 flex rounded-lg">
                <svg viewBox="0 0 20 20" fill="currentColor" class="menu w-6 h-6"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
                <span class="px-2 py-1">Pages</span>
                <svg viewBox="0 0 20 20" fill="currentColor" class="chevron-down w-6 h-6 right-4 absolute"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </a>
            <ul x-show.transition.ease-out.duration-100.transform.opacity-0.scale-95.transform.opacity-100.scale-100.transition.ease-in.duration-75.transform.opacity-100.scale-100.transform.opacity-0.scale-95="open"
            @click.away="open = false">
                <x-item type="mainItem">
                    <a href="" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>sign-in-alt</title><g fill=""><path d="M20.5 15.1a1 1 0 0 0-1.34.45A8 8 0 1 1 12 4a7.93 7.93 0 0 1 7.16 4.45 1 1 0 0 0 1.8-.9 10 10 0 1 0 0 8.9 1 1 0 0 0-.46-1.35zM21 11h-9.59l2.3-2.29a1 1 0 1 0-1.42-1.42l-4 4a1 1 0 0 0-.21.33 1 1 0 0 0 0 .76 1 1 0 0 0 .21.33l4 4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L11.41 13H21a1 1 0 0 0 0-2z"></path></g></svg>
                        <span class="px-2 py-1">Login</span>
                    </a>
                </x-item>
                <x-item type="mainItem">
                    <a href="" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="fill-current" viewBox="0 0 24 24"><title>user-plus</title><g fill=""><path d="M21 10.5h-1v-1a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0v-1h1a1 1 0 0 0 0-2zm-7.7 1.72A4.92 4.92 0 0 0 15 8.5a5 5 0 0 0-10 0 4.92 4.92 0 0 0 1.7 3.72A8 8 0 0 0 2 19.5a1 1 0 0 0 2 0 6 6 0 0 1 12 0 1 1 0 0 0 2 0 8 8 0 0 0-4.7-7.28zM10 11.5a3 3 0 1 1 3-3 3 3 0 0 1-3 3z"></path></g></svg>
                        <span class="px-2 py-1">Register</span>
                    </a>
                </x-item>
                <x-item type="mainItem">
                    <a href="" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>user</title><g fill=""><path d="M15.71 12.71a6 6 0 1 0-7.42 0 10 10 0 0 0-6.22 8.18 1 1 0 0 0 2 .22 8 8 0 0 1 15.9 0 1 1 0 0 0 1 .89h.11a1 1 0 0 0 .88-1.1 10 10 0 0 0-6.25-8.19zM12 12a4 4 0 1 1 4-4 4 4 0 0 1-4 4z"></path></g></svg>
                        <span class="px-2 py-1">Profile</span>
                    </a>
                </x-item>
                <x-item type="mainItem">
                    <a href="" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>user</title><g fill=""><path d="M15.71 12.71a6 6 0 1 0-7.42 0 10 10 0 0 0-6.22 8.18 1 1 0 0 0 2 .22 8 8 0 0 1 15.9 0 1 1 0 0 0 1 .89h.11a1 1 0 0 0 .88-1.1 10 10 0 0 0-6.25-8.19zM12 12a4 4 0 1 1 4-4 4 4 0 0 1-4 4z"></path></g></svg>
                        <span class="px-2 py-1">Github</span>
                    </a>
                </x-item>
            </ul>
        </div> -->

        <a href="{{ route('admin.home') }}" class="group hover:bg-custom-400 mx-2 text-gray-300 text-sm hover:text-white px-2 py-2 flex rounded-lg" >
             <svg viewBox="0 0 20 20" fill="currentColor" class="menu w-6 h-6"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
            <span class="mx-2">Dashboard</span>
        </a>

        @can('content_access')
        <div x-data="{ open: false }">
            <a href="#" @click="open=true" class="{{ request()->is("admin/pages*")||request()->is("admin/posts*")||request()->is("admin/comments*")||request()->is("admin/images*")||request()->is("admin/galleries*") ? "bg-custom-500 text-white" : "text-gray-300" }} group hover:bg-custom-400 mx-2 text-sm hover:text-white px-2 py-2 flex rounded-lg relative ">
                <svg viewBox="0 0 20 20" fill="currentColor" class="menu w-6 h-6"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
                <span class="px-2 py-1">Content</span>
                <svg viewBox="0 0 20 20" fill="currentColor" class="chevron-down w-6 h-6  absolute right-2"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </a>
            <ul x-show.transition.ease-out.duration-100.transform.opacity-0.scale-95.transform.opacity-100.scale-100.transition.ease-in.duration-75.transform.opacity-100.scale-100.transform.opacity-0.scale-95="open"
            @click.away="open = false">
                <x-item type="mainItem">
                    <a href="{{ route("admin.pages.index") }}" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>sign-in-alt</title><g fill=""><path d="M20.5 15.1a1 1 0 0 0-1.34.45A8 8 0 1 1 12 4a7.93 7.93 0 0 1 7.16 4.45 1 1 0 0 0 1.8-.9 10 10 0 1 0 0 8.9 1 1 0 0 0-.46-1.35zM21 11h-9.59l2.3-2.29a1 1 0 1 0-1.42-1.42l-4 4a1 1 0 0 0-.21.33 1 1 0 0 0 0 .76 1 1 0 0 0 .21.33l4 4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L11.41 13H21a1 1 0 0 0 0-2z"></path></g></svg>
                        <span class="px-2 py-1">Pages</span>
                    </a>
                </x-item>
                <x-item type="mainItem">
                    <a href="{{ route("admin.posts.index") }}" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>sign-in-alt</title><g fill=""><path d="M20.5 15.1a1 1 0 0 0-1.34.45A8 8 0 1 1 12 4a7.93 7.93 0 0 1 7.16 4.45 1 1 0 0 0 1.8-.9 10 10 0 1 0 0 8.9 1 1 0 0 0-.46-1.35zM21 11h-9.59l2.3-2.29a1 1 0 1 0-1.42-1.42l-4 4a1 1 0 0 0-.21.33 1 1 0 0 0 0 .76 1 1 0 0 0 .21.33l4 4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L11.41 13H21a1 1 0 0 0 0-2z"></path></g></svg>
                        <span class="px-2 py-1">Posts</span>
                    </a>
                </x-item>
            </ul>
        </div>
        @endcan

        @can('content_access')
        <a href="{{ route('admin.newsletters.index') }}" class="{{request()->is("admin/newsletter*") ? "bg-custom-500 text-white" : "text-gray-300" }} group hover:bg-custom-400 mx-2 text-sm hover:text-white px-2 py-2 flex rounded-lg relative ">
             <svg viewBox="0 0 20 20" fill="currentColor" class="menu w-6 h-6"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
            <span class="mx-2">Newsletter</span>
        </a>
        @endcan
<!-- 
        @can('content_access')
        <div x-data="{ open: false }">
            <a href="#" @click="open=true" class="{{request()->is("admin/newsletter*") ? "bg-custom-500 text-white" : "text-gray-300" }} group hover:bg-custom-400 mx-2 text-sm hover:text-white px-2 py-2 flex rounded-lg relative ">
                <svg viewBox="0 0 20 20" fill="currentColor" class="menu w-6 h-6"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
                <span class="px-2 py-1">Newsletter</span>
                <svg viewBox="0 0 20 20" fill="currentColor" class="chevron-down w-6 h-6  absolute right-2"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </a>
            <ul x-show.transition.ease-out.duration-100.transform.opacity-0.scale-95.transform.opacity-100.scale-100.transition.ease-in.duration-75.transform.opacity-100.scale-100.transform.opacity-0.scale-95="open"
            @click.away="open = false">
                <x-item type="mainItem">
                    <a href="{{ route("admin.newsletters.index") }}" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>sign-in-alt</title><g fill=""><path d="M20.5 15.1a1 1 0 0 0-1.34.45A8 8 0 1 1 12 4a7.93 7.93 0 0 1 7.16 4.45 1 1 0 0 0 1.8-.9 10 10 0 1 0 0 8.9 1 1 0 0 0-.46-1.35zM21 11h-9.59l2.3-2.29a1 1 0 1 0-1.42-1.42l-4 4a1 1 0 0 0-.21.33 1 1 0 0 0 0 .76 1 1 0 0 0 .21.33l4 4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L11.41 13H21a1 1 0 0 0 0-2z"></path></g></svg>
                        <span class="px-2 py-1">Newsletters</span>
                    </a>
                </x-item>
                <x-item type="mainItem">
                    <a href="{{ route("admin.posts.index") }}" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>sign-in-alt</title><g fill=""><path d="M20.5 15.1a1 1 0 0 0-1.34.45A8 8 0 1 1 12 4a7.93 7.93 0 0 1 7.16 4.45 1 1 0 0 0 1.8-.9 10 10 0 1 0 0 8.9 1 1 0 0 0-.46-1.35zM21 11h-9.59l2.3-2.29a1 1 0 1 0-1.42-1.42l-4 4a1 1 0 0 0-.21.33 1 1 0 0 0 0 .76 1 1 0 0 0 .21.33l4 4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L11.41 13H21a1 1 0 0 0 0-2z"></path></g></svg>
                        <span class="px-2 py-1">Posts</span>
                    </a>
                </x-item>
            </ul>
        </div>
        @endcan -->

        @can('content_access')
        <div x-data="{ open: false }">
            <a href="#" @click="open=true" class="{{ request()->is("admin/permissions*")||request()->is("admin/roles*")||request()->is("admin/users*") ? "bg-custom-500 text-white" : "text-gray-300" }} group hover:bg-custom-400 mx-2 text-sm hover:text-white px-2 py-2 flex rounded-lg relative">
                <svg viewBox="0 0 20 20" fill="currentColor" class="menu w-6 h-6"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
                <span class="px-2 py-1">Users</span>
                <svg viewBox="0 0 20 20" fill="currentColor" class="chevron-down w-6 h-6 absolute right-2"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </a>
            <ul x-show.transition.ease-out.duration-100.transform.opacity-0.scale-95.transform.opacity-100.scale-100.transition.ease-in.duration-75.transform.opacity-100.scale-100.transform.opacity-0.scale-95="open"
            @click.away="open = false">
                @can('permission_access')
                <x-item type="mainItem">
                    <a href="{{ route('admin.permissions.index') }}" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" width="24" height="24" viewBox="0 0 24 24"><title>sign-in-alt</title><g fill=""><path d="M20.5 15.1a1 1 0 0 0-1.34.45A8 8 0 1 1 12 4a7.93 7.93 0 0 1 7.16 4.45 1 1 0 0 0 1.8-.9 10 10 0 1 0 0 8.9 1 1 0 0 0-.46-1.35zM21 11h-9.59l2.3-2.29a1 1 0 1 0-1.42-1.42l-4 4a1 1 0 0 0-.21.33 1 1 0 0 0 0 .76 1 1 0 0 0 .21.33l4 4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L11.41 13H21a1 1 0 0 0 0-2z"></path></g></svg>
                        <span class="px-2 py-1">Permissions</span>
                    </a>
                </x-item>
                @endcan
                @can('role_access')
                <x-item type="mainItem">
                    <a href="{{ route('admin.roles.index') }}" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="fill-current" viewBox="0 0 24 24"><title>user-plus</title><g fill=""><path d="M21 10.5h-1v-1a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0v-1h1a1 1 0 0 0 0-2zm-7.7 1.72A4.92 4.92 0 0 0 15 8.5a5 5 0 0 0-10 0 4.92 4.92 0 0 0 1.7 3.72A8 8 0 0 0 2 19.5a1 1 0 0 0 2 0 6 6 0 0 1 12 0 1 1 0 0 0 2 0 8 8 0 0 0-4.7-7.28zM10 11.5a3 3 0 1 1 3-3 3 3 0 0 1-3 3z"></path></g></svg>
                        <span class="px-2 py-1">Roles</span>
                    </a>
                </x-item>
                @endcan
                @can('content_access')
                <x-item type="mainItem">
                    <a href="{{ route('admin.users.index') }}" class="flex px-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="fill-current" viewBox="0 0 24 24"><title>user-plus</title><g fill=""><path d="M21 10.5h-1v-1a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0v-1h1a1 1 0 0 0 0-2zm-7.7 1.72A4.92 4.92 0 0 0 15 8.5a5 5 0 0 0-10 0 4.92 4.92 0 0 0 1.7 3.72A8 8 0 0 0 2 19.5a1 1 0 0 0 2 0 6 6 0 0 1 12 0 1 1 0 0 0 2 0 8 8 0 0 0-4.7-7.28zM10 11.5a3 3 0 1 1 3-3 3 3 0 0 1-3 3z"></path></g></svg>
                        <span class="px-2 py-1">Users</span>
                    </a>
                </x-item>
                @endcan
            </ul>
        </div>
        @endcan
            
            
<!--             <ul class=""> -->

                


              <!--   @can('content_access')

                    <li class="px-2 py-3 bg-gray-900 rounded">
                        <a class="has-sub {{ request()->is("admin/pages*")||request()->is("admin/posts*")||request()->is("admin/comments*")||request()->is("admin/images*")||request()->is("admin/galleries*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="#" onclick="window.openSubNav(this)">
                            <i class="fa-fw fas fa-users text-gray-300">
                            </i>
                            <span class="mx-2 text-gray-300">Content</span>
                        </a>
                        <ul class="ml-4 subnav hidden px-2 py-3 bg-gray-900 rounded">
                            @can('page_access')
                                <li class="py-2 flex items-center">
                                    <a class="{{ request()->is("admin/pages*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.pages.index") }}">
                                        <i class="fa-fw text-gray-300 fas fa-cogs">
                                        </i>
                                        <span class="mx-2 text-gray-300">Page</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
 -->
            
                <!-- @can('user_management_access')
                    <li class="px-2 py-3 bg-gray-900 rounded">
                        <a class="has-sub {{ request()->is("admin/permissions*")||request()->is("admin/roles*")||request()->is("admin/users*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="#" onclick="window.openSubNav(this)">
                            <i class="fa-fw fas fa-users text-gray-300">
                            </i>
                            <span class="mx-2 text-gray-300">Users</span>
                        </a>
                        <ul class="ml-4 subnav hidden">
                            @can('permission_access')
                                <li class="py-2 flex items-center">
                                    <a class="{{ request()->is("admin/permissions*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.permissions.index") }}">
                                        <i class="fa-fw text-gray-300 fas fa-unlock-alt">
                                        </i>
                                        <span class="mx-2 text-gray-300">{{ trans('cruds.permission.title') }}</span>
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="py-2 flex items-center">
                                    <a class="{{ request()->is("admin/roles*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.roles.index") }}">
                                        <i class="fa-fw text-gray-300 fas fa-briefcase">
                                        </i>
                                        <span class="mx-2 text-gray-300">{{ trans('cruds.role.title') }}</span>
                                    </a>
                                </li>
                            @endcan
                            @can('user_access')
                                <li class="py-2 flex items-center">
                                    <a class="{{ request()->is("admin/users*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.users.index") }}">
                                        <i class="fa-fw text-gray-300 fas fa-user">
                                        </i>
                                        <span class="mx-2 text-gray-300">{{ trans('cruds.user.title') }}</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan -->
<!-- 
                @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                    @can('profile_password_edit')
                        <li class="px-2 py-3 bg-gray-900 rounded">
                            <a href="{{ route("profile.password.edit") }}" class="{{ request()->is("profile/password") || request()->is("profile/password/*") ? "sidebar-nav-active" : "sidebar-nav" }}">
                                <i class="fas fa-cogs text-gray-300"></i>
                                <span class="mx-2 text-gray-300">{{ trans('global.change_password') }}</span>
                            </a>
                        </li>
                    @endcan
                @endif -->

<!-- 
            </ul> -->
            
 <!--        <ul>
            <li class="px-2 py-3 bg-gray-900 rounded">
                <a href="#" class="flex items-center">
                    <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                         stroke-linejoin="round"
                         stroke-width="2"
                         viewBox="0 0 24 24" stroke="currentColor">
                        <path
                            d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
                    </svg>
                    <span class="mx-2 text-gray-300">Dashboard</span>
                </a>
            </li>
            <li class="px-2 py-3 hover:bg-gray-900 rounded mt-2">
                <a href="#" class="flex items-center">
                    <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                         stroke-linejoin="round"
                         stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path
                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path>
                    </svg>
                    <span class="mx-2 text-gray-300">Team</span>
                </a>
            </li>
            <li class="px-2 py-3 hover:bg-gray-900 rounded mt-2">
                <a href="#" class="flex items-center">
                    <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                         stroke-linejoin="round"
                         stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path
                            d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"></path>
                    </svg>
                    <span class="mx-2 text-gray-300">Projects</span>
                </a>
            </li>
            <li class="px-2 py-3 hover:bg-gray-900 rounded mt-2">
                <a href="#" class="flex items-center">
                    <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                         stroke-linejoin="round"
                         stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path
                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                    </svg>
                    <span class="mx-2 text-gray-300">Calendar</span>
                </a>
            </li>
            <li class="px-2 py-3 hover:bg-gray-900 rounded mt-2">
                <a href="#" class="flex items-center">
                    <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                         stroke-linejoin="round"
                         stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path
                            d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4"></path>
                    </svg>
                    <span class="mx-2 text-gray-300">Documents</span>
                </a>
            </li>
            <li class="px-2 py-3 hover:bg-gray-900 rounded mt-2">
                <a href="#" class="flex items-center">
                    <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                         stroke-linejoin="round"
                         stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path
                            d="M16 8v8m-4-5v5m-4-2v2m-2 4h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                    </svg>
                    <span class="mx-2 text-gray-300">Reports</span>
                </a>
            </li>
        </ul> -->
       
    </div>
</aside>
