@foreach($frontmenu as $menuItem)
    <a class="mx-2 text-lg hover:text-custom-500" href="/{{ $menuItem->slug }}">{!! $menuItem->title !!}</a>
@endforeach  