<form wire:submit.prevent="submit" class="pt-3">

    <x-input.group for="name" label="Role" :error="$errors->first('role.title')">
        <x-input.text name="title" id="title" required wire:model.defer="role.title" placeholder="Title" />
    </x-input.group>

    <x-input.group x-cloak for="permissions" label="Permissions" :error="$errors->first('permissions')">
                    <x-input.multiple-select wire:model.defer="permissions" id="permissions" :options="$allpermissions" />
                </x-input.group>


    

    <div class="form-group">
        <x-button.primary type="submit">Save</x-button.primary>
     
        <a href="{{ route('admin.roles.index') }}">
             <x-button.secondary>Cancel</x-button.secondary>
        </a>
    </div>
</form>