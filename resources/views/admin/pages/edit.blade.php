@extends('layouts.admin')

@section('styles')
<!--     <link href="/css/bootstrap-grid.css" rel="stylesheet" type="text/css" /> -->
<!--     <link href="/assets/minimalist-blocks/content.css" rel="stylesheet" type="text/css" /> -->
    <link href="/contentbuilder/contentbuilder.css" rel="stylesheet" type="text/css" />
<!--     <link href="/assets/minimalist-blocks/in_style.css" rel="stylesheet" type="text/css" /> -->
@endsection

@section('content')

 <h1 class="text-2xl font-medium text-gray-700">Pages: Edit</h1>
<div class="is_container min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg bg-white p-3 mt-5" >
    <div id="contentarea" >
        {!! $page->content !!}
    </div>
</div>

<div class="min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg bg-white p-5 mt-5">

    <x-form.open action="{{route('admin.pages.update', $page->id)}}" id="form">
        @method('PUT')

        @include('admin.pages.forms')

    </x-form.open>



    <div class="box-footer mt20">
        <span class="inline-flex rounded-md shadow-sm">
            <button onclick="save()" class="mt-3 py-2 px-4 border rounded-md text-sm leading-5 font-medium focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition duration-150 ease-in-out text-white bg-custom-600 hover:bg-custom-500 active:bg-custom-700 border-custom-600"> Save</button>
        </span>       
    </div>
</div>




@endsection

@push('scripts')

<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
<script src="/contentbuilder/contentbuilder.js" type="text/javascript"></script>
<script src="/contentbuilder/saveimages.js" type="text/javascript"></script>

<script type="text/javascript">
    var builder = new ContentBuilder({
        container: '#contentarea',
        snippetData: '/admin/snippets',
        rowFormat: '<section class="py-5"><div class="container max-w-6xl mx-auto mb-4"></div></div>',
        builderMode: '',
        imageselect: '/assets/images.html',
        toolbar: 'top',
        toolbarDisplay: 'auto',
        columnTool: true,
        rowTool: 'right',
        elementTool: true,
        snippetAddTool: true,
        outlineMode: '',
        rowcolOutline: true,
        outlineStyle: '',
        elementHighlight: true,
        snippetOpen: false,
        toolStyle: '',
        snippetsSidebarDisplay: 'auto',
        clearPreferences: true, //reset settings on load
        //See readme.txt for more
    });

    function save() {
        //Save Images
        $("#contentarea").saveimages({
            handler: '/saveimage.php',
            onComplete: function () {

            var sHTML = builder.html(); // Get content

            $('#content').val(sHTML);

            $( "#form" ).submit();

            }
        });
        $("#contentarea").data('saveimages').save();
    }
</script>
<script>
  
function meetupData () {
  return {
    title: '[Meetup Events List]',
    events: [],
  
    init () {
      this.getEvents()
    },

    getEvents () {
      // fetch('/api/meetup')
      //   .then(response => response.json())
      //   .then(data => this.events = data)
    },
  }
}

function postData () {
  return {
    title: '[Posts List]',
    posts: [],
  
    init () {
      this.getPosts()
    },

    getPosts () {
      // fetch('/api/meetup')
      //   .then(response => response.json())
      //   .then(data => this.events = data)
    },
  }
}
</script>
@endpush