@props([
    'leadingIcon' => false,
    'resetFilters' => false,
])

<div class="relative">
	@if ($leadingIcon)
        <div class="absolute top-1 left-2"> 
	        <x-icon.search class="text-gray-400 h-4 w-4"/> 
	    </div> 
    @endif

    <x-input.text {{ $attributes->merge(['class' => 'h-10 w-60 pl-8']) }} />

    @if ($resetFilters)
        <div class="absolute top-2 right-2"> 
	        <button class="text-xs border focus:outline-none bg-gray-200 border-gray-300 hover:border-gray-400 py-1 px-2 text-gray-700 rounded-md" wire:click="{{ $resetFilters }}">Clear</button> 
	    </div>
    @endif
</div>