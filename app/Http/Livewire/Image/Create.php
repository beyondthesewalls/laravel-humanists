<?php

namespace App\Http\Livewire\Image;

use App\Models\Image;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Create extends Component
{
    public Image $image;

    public array $mediaToRemove = [];

    public array $mediaCollections = [];

    public function mount(Image $image)
    {
        $this->image = $image;
    }

    public function render()
    {
        return view('livewire.image.create');
    }

    public function submit()
    {
        $this->validate();

        $this->image->save();
        $this->syncMedia();

        return redirect()->route('admin.images.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    protected function rules(): array
    {
        return [
            'image.title' => [
                'string',
                'required',
            ],
            'mediaCollections.image_file' => [
                'array',
                'nullable',
            ],
            'mediaCollections.image_file.*.id' => [
                'integer',
                'exists:media,id',
            ],
        ];
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->image->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
