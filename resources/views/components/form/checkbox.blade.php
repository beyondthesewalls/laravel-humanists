@props([
    'name' => '',
    'label' => '',
    'value' => '1',
])

@if ($label === '')
    @php
        //remove underscores from name
        $label = str_replace('_', ' ', $name);
        //detect subsequent letters starting with a capital 
        $label = preg_split('/(?=[A-Z])/', $label);
        //display capital words with a space
        $label = implode(' ', $label);
        //uppercase first letter and lower the rest of a word
        $label = ucwords(strtolower($label));
    @endphp
@endif

<div>
    <label for='{{ $name }}' class="inline-flex items-center">
        <input type="hidden" name="{{ $name }}" value='0'>
        <input type='checkbox' name='{{ $name }}' value='{{ $value }}' @if ($slot != '') checked="checked" @endif {!! $attributes->merge([
            'class' => 'form-checkbox h-5 w-5 text-custom-600 transition duration-150 ease-in-out' . ($label ? ' mt-1' : '')
        ]) !!}>
        <span class="ml-2 text-sm font-medium text-gray-700">{{ $label }}</span>
    </label>
</div>