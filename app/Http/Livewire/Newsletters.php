<?php

namespace App\Http\Livewire;

use App\Models\Newsletterdraft;
use App\Models\Template;
use Livewire\Component;
use Livewire\WithPagination;
use App\CustomClass\PaginationMerger;
use \Illuminate\Session\SessionManager;
use Carbon\Carbon;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

use Newsletter;

use App\Helpers\InteractsWithBanner;

class Newsletters extends Component

{
    use WithPagination,InteractsWithBanner;

    public $search = '';
    public $filter = 10;
    public $sortField = 'create_time';
    public $sortDirection = 'desc';
    public $clearSearch;


    public $showDeleteSingleModal = false;
    public $showTestModal = false;
    public $showSendModal = false;
    public $deleteID = null;
    public $testID = null;
    public $sendID = null;

    public $testemail = '';

    protected $queryString = [];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingFilter()
    {
        $this->resetPage();
    }

    public function clearSearch(): void
     {
         $this->search = '';
     }


    public function sortBy($field)
    {
       if ($this->sortField === $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }

        $this->sortField = $field;
    }

    public function render()
    {

        $response = Newsletter::getApi()->get('campaigns', ['count'=> 20, 'sort_field' => 'create_time', 'sort_dir' => 'DESC' ]); 

        $pages = collect($response['campaigns']);

        if ($this->search != '') {
            $pages = collect($response->campaigns)->filter(function ($value, $key) {
                if (strpos($value->settings->title, $this->search) !== false) {
                    return $value;
                }
            });
        }


        if ($this->sortDirection == 'asc') {
            $pages = $pages->sortBy($this->sortField);
        } else {
            $pages = $pages->sortByDesc($this->sortField);
        }


        $pages = $pages->paginate($this->filter);

        // session()->flash('message', 'Test email sent.');

              // $pagination = $this->filter;

        // $vacancies = $vacancies->paginate($pagination);
 
        return view('livewire.newsletters', compact('pages'));
    }

    public function delete($id)
    {
        $this->deleteID = $id;

        $this->showDeleteSingleModal = true;
    }

    public function test($id)
    {
        $this->testID = $id;

        $this->showTestModal = true;
    }

    public function send($id)
    {
        $this->sendID = $id;

        $this->showSendModal = true;
    }

    public function deleteSingle()
    {
        $this->showDeleteSingleModal = false;

        $client = new \MailchimpMarketing\ApiClient();

        $client->setConfig([
          'apiKey' => 'c83e916712d151c5993c27d18ed7f922',
          'server' => 'us6',
        ]);

        $transaction = $client->campaigns->remove($this->deleteID);

        $newsletter = Newsletterdraft::where('mailchimp_id', $this->deleteID)->first();
        
        if ($newsletter != '') {
            $newsletter->delete();
        }

        $this->notify('You\'ve deleted a newsletter');
    }

    public function sendTest()
    {
        $this->showTestModal = false;

        $client = new \MailchimpMarketing\ApiClient();

        $client->setConfig([
          'apiKey' => '22e0fd754e7e1247d7a0bdb8404ff03e',
          'server' => 'us4'
        ]);

        // $response = $client->campaigns->sendTestEmail($this->testID, [
        //     "test_emails" => [$this->testemail],
        //     "send_type" => "html",
        // ]);

        session()->flash('message', 'Test email sent.');
    }

    public function sendEmail()
    {
        $this->showSendModal = false;

        $client = new \MailchimpMarketing\ApiClient();

        $client->setConfig([
          'apiKey' => '22e0fd754e7e1247d7a0bdb8404ff03e',
          'server' => 'us4'
        ]);

        // $response = $client->campaigns->sendTestEmail($this->testID, [
        //     "test_emails" => [$this->testemail],
        //     "send_type" => "html",
        // ]);

        session()->flash('message', 'Newsletter sent.');

        $this->mount();
    }

    
} 



// <?php

// namespace App\Http\Livewire;

// use Livewire\Component;
// use App\Models\Page;
// use App\Models\Newsletter;
// use Illuminate\Support\Carbon;
// use App\Http\Livewire\DataTable\WithSorting;
// use App\Http\Livewire\DataTable\WithCachedRows;
// use App\Http\Livewire\DataTable\WithBulkActions;
// use App\Http\Livewire\DataTable\WithPerPagePagination;

// use Illuminate\Support\Arr;
// use Illuminate\Support\Collection;


// class Newsletters extends Component
// {
//     use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

//     public $showDeleteModal = false;
//     public $showDeleteSingleModal = false;
//     public $deleteID = null;

//     public $showEditModal = false;
//     public $showFilters = false;
//     public $filters = [
//         'search' => '',
//         'status' => '',
//         'amount-min' => null,
//         'amount-max' => null,
//         'date-min' => null,
//         'date-max' => null,
//     ];
//     public Page $editing;

//     public array $locationUsers = [];

//     public function locationUsersSelected($locationUsersValues)
//     {
//       $this->locationUsers = $locationUsersValues;
//     }

//     protected $queryString = ['sorts'];

//     protected $listeners = ['refreshTransactions' => '$refresh', 'locationUsersSelected'];

//     public function rules() { return [
//         'editing.title' => 'required|min:3',
//         'editing.amount' => 'required',
//         'editing.status' => 'required|in:'.collect(Page::STATUSES)->keys()->implode(','),
//         'editing.date_for_editing' => 'required',
//     ]; }

//     public function mount() { $this->editing = $this->makeBlankTransaction(); }
//     public function updatedFilters() { $this->resetPage(); }

//     public function exportSelected()
//     {
//         return response()->streamDownload(function () {
//             echo $this->selectedRowsQuery->toCsv();
//         }, 'transactions.csv');
//     }

//     public function deleteSelected()
//     {
//         $deleteCount = $this->selectedRowsQuery->count();

//         $this->selectedRowsQuery->delete();

//         $this->showDeleteModal = false;

//         $this->notify('You\'ve deleted '.$deleteCount.' transactions');
//     }

//     public function deleteSingle()
//     {
//         $this->showDeleteSingleModal = false;

//         $transaction = Page::findOrFail($this->deleteID)->delete();

//         $this->notify('You\'ve deleted a transaction');
//     }

//     public function makeBlankTransaction()
//     {
//         return Page::make(['date' => now(), 'status' => 'success']);
//     }

//     public function toggleShowFilters()
//     {
//         $this->useCachedRows();

//         $this->showFilters = ! $this->showFilters;
//     }
    

//     public function create()
//     {
//         $this->useCachedRows();

//         if ($this->editing->getKey()) $this->editing = $this->makeBlankTransaction();

//         $this->showEditModal = true;
//     }

//     public function edit(Page $transaction)
//     {
//         $this->useCachedRows();

//         if ($this->editing->isNot($transaction)) $this->editing = $transaction;

//         $this->showEditModal = true;
//     }

//     public function save()
//     {
//         $this->validate();

//         $this->editing->save();

//         $this->showEditModal = false;
//     }

//     public function resetFilters() { $this->reset('filters'); }

//     public function getRowsQueryProperty()
//     {
//         $client = new \MailchimpMarketing\ApiClient();

//         $client->setConfig([
//           'apiKey' => 'ea48ef41fea9b7832b64ae22034237b4',
//           'server' => 'us6'
//         ]);

//         $response = $client->campaigns->list();

//         $collection = collect($response->campaigns);
//         // $collection = $collection->where('web_id', '>', '1' )->sortBy('create_time');

//         // $collection =  $collection->values()->all();

//         $query = $collection
//             ->when($this->filters['status'], fn($query, $status) => $query->where('status', $status))
//             ->when($this->filters['search'], fn($query, $search) => $query->where('settings.title', 'like', '%'.$search.'%'));

//         // $decodedAsArray = Arr::flatten($response->campaigns);

//         return $this->applySorting($query);
//     }

//     public function getRowsProperty()
//     {
//         return $this->cache(function () {
//             return $this->rowsQuery;
//         });
//     }

//     public function render()
//     {
//         return view('livewire.newsletters', [
//             'pages' => $this->rows,
//         ]);
//     }

//     public function delete($id)
//     {
//         $this->useCachedRows();

//         $this->deleteID = $id;

//         $this->showDeleteSingleModal = true;
//     }


// }
