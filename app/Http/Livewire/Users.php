<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Carbon;
use App\Http\Livewire\DataTable\WithSorting;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use Illuminate\Support\Str;

class Users extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public $showDeleteModal = false;
    public $showDeleteSingleModal = false;
    public $deleteID = null;
    public $role = 0;
    public $search;

    public $showEditModal = false;
    public $showFilters = false;
    public $filters = [
        'search' => '',
        'status' => '',
        'amount-min' => null,
        'amount-max' => null,
        'date-min' => null,
        'date-max' => null,
    ];
    public User $editing;

    
    protected $queryString = ['sorts'];

    public array $roles = [];
    public $myroles = '';
    public array $listsForFields = [];

    protected $listeners = ['refreshTransactions' => '$refresh'];

    public function rules() { return [
        'editing.name' => 'required|min:3',
        'editing.payment_date' => 'required|min:3',
        
  
    ]; }

    public function clearSearch(): void
     {
         $this->search = '';
     }

    public function mount() { 
        $this->editing = $this->makeBlankTransaction(); 
        $this->role = request()->query('role', $this->role);
    }
    public function updatedFilters() { $this->resetPage(); }

    public function exportSelected()
    {
        return response()->streamDownload(function () {
            echo $this->selectedRowsQuery->toCsv();
        }, 'transactions.csv');
    }

    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->delete();

        $this->showDeleteModal = false;

        $this->notify('You\'ve deleted '.$deleteCount.' transactions');
    }

    public function deleteSingle()
    {
        $this->showDeleteSingleModal = false;

        $transaction = User::findOrFail($this->deleteID)->delete();

        $this->notify('You\'ve deleted a transaction');
    }

    public function makeBlankTransaction()
    {
        return User::make(['date' => now(), 'status' => 'success']);
    }

    public function toggleShowFilters()
    {
        $this->useCachedRows();

        $this->showFilters = ! $this->showFilters;
    }

    public function create()
    {
        $this->useCachedRows();
        $this->roles = [];

        if ($this->editing->getKey()) $this->editing = $this->makeBlankTransaction();

        $this->showEditModal = true;
    }

    public function edit(User $user)
    {
        $this->useCachedRows();

        $this->editing = $user;
        $this->roles = $user->roles()->pluck('id')->toArray();
 
        $this->showEditModal = true;
    }

    public function save()
    {
        $this->validate();

        $this->editing->save();
        $this->editing->roles()->sync($this->roles);

        $this->showEditModal = false;
    }

    public function resetFilters() { $this->reset('filters'); }

    public function getRowsQueryProperty()
    {
        $findrole = [$this->role];

        $query = User::query()
            // ->when($this->filters['name'], fn($query, $status) => $query->where('name', $status))
            ->when($this->filters['search'], fn($query, $search) => $query->findUserByName($search))
            ->when($this->role, function ($query) use ($findrole) {
                $query->whereHas('roles', function ($q) use ($findrole) {
                    $q->whereIn('id', $findrole);
                    
                });
            });

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        $findrole = [$this->role];


        $this->myroles = Role::all();


        return view('livewire.users', [
            'users' => $this->rows,
        ]);
    }

    public function delete($id)
    {
        $this->useCachedRows();

        $this->deleteID = $id;

        $this->showDeleteSingleModal = true;
    }



}
