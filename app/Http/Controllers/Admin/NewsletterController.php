<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Newsletterdraft;
use App\Models\User;
use App\Models\Template;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\MeetupService;
use Carbon\Carbon;
use Newsletter;
use \DrewM\MailChimp\MailChimp;


class NewsletterController extends Controller
{

    public function index()
    {
        // $response = Newsletter::getApi()->get('campaigns', ['count'=> 10, 'sort_field' => 'create_time', 'sort_dir' => 'DESC' ]); 

        // return $response;

        

        // $response = Newsletter::getApi()->get('campaigns', ['count'=> 20, 'sort_field' => 'create_time', 'sort_dir' => 'DESC' ]); 

        // return $response;

        // abort_if(Gate::denies('page_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $client = new \MailchimpMarketing\ApiClient();
        // $client->setConfig([
        //     'apiKey' => env("MAILCHIMP_KEY"),
        //     'server' => env("MAILCHIMP_Region"),
        // ]);

        // $response = $client->campaigns->list();
        // $pages = $response->campaigns;

        // $collection = collect($pages);


        // return $response;

        // // $pages = $response->campaigns;

        // // $newsletterdraft = Newsletterdraft::all();

        // $sorted = $collection->sortByDesc('web_id');

        // return $sorted->values()->all();

        // $response = Newsletter::getApi()->get('lists'); 
        // $list_id = 'a007760c2a';

        // $response = Newsletter::getApi()->get('lists/'.$list_id.'/segments'); 

        // $response = Newsletter::getApi()->get( 'campaigns', [
        //     'count' => 5,
        //     'sort_field' => 'create_time',
        //     'sort_dir' => 'DESC',
        // ]);

        // $MailChimp = new MailChimp('c83e916712d151c5993c27d18ed7f922-us6');

        

        // $response = $MailChimp->get('lists/'.$list_id.'/segments');
        // $response = $MailChimp->get( 'campaigns', [
        //     'count' => 100
        // ]);
// unique_opens
// clicks

        // return $response;


        return view('admin.newsletters.index');
    }

    public function create()
    {
        // $response = Newsletter::getApi()->get('lists');    
        // return $response;


        // $collection = Newsletter::getApi()->get('lists/a007760c2a/interest-categories/f7ce0f15a0/interests/'); 
        // return Newsletter::getApi()->get('campaigns'); 


        // $collection = Newsletter::getApi()->get('lists/a007760c2a/segments'); 
        // $interests =  $collection['segments'];

        $response = Newsletter::getApi()->get('lists');
        $interests = $response['lists'];

        // return $interests;

        $newsletterdraft = new Newsletterdraft(['subject' => ' Newsletter', 'user_id' => 2, 'from_email' => 'info@centrallondonhumanists.org.uk']);
        $users = User::orderBy('name')->get()->pluck('full_name', 'full_name');

        return view('admin.newsletters.create', compact('newsletterdraft', 'users', 'interests'));
    }



    public function store(Request $request)
    {
        

        $template = Template::find(1);
        $replace = ['[[Content]]' => $request->content];
        $string = $template->content;
        $mailcontent = str_replace(array_keys($replace), array_values($replace), $string);

        $response = Newsletter::getApi()->post("campaigns", [ 
            'type' => 'regular', 
            'recipients' => ['list_id' =>  $request->list_id], 
            'settings' => [
                'subject_line' => $request->subject, 
                'title' => $request->subject, 
                'reply_to' => $request->from_email, 
                'from_name' => $request->from_name] 
            ]);


        $content = Newsletter::getApi()->put("campaigns/".$response['id']."/content", [ 
            'html' => $mailcontent
            ]);

        $page = Newsletterdraft::create(['mailchimp_id' => $response['id'] ] + $request->all());
    

        return redirect()->route('admin.newsletters.index');
    }

    public function edit($id)
    {
        // abort_if(Gate::denies('post_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $newsletterdraft = Newsletterdraft::where('mailchimp_id', $id)->first();

        $response = Newsletter::getApi()->get('lists');
        $interests = $response['lists'];

        $users = User::orderBy('name')->get()->pluck('full_name', 'full_name');

        return view('admin.newsletters.edit', compact('newsletterdraft', 'users', 'interests'));
    }

    public function update(Request $request, $id)
    {
        $newsletterdraft = Newsletterdraft::where('mailchimp_id', $id)->first();
        $newsletterdraft->update($request->all());


        $template = Template::find(1);
        $replace = ['[[Content]]' => $request->content];
        $string = $template->content;
        $mailcontent = str_replace(array_keys($replace), array_values($replace), $string);

        

        
        $response = Newsletter::getApi()->patch("campaigns/".$id, [ 
            'recipients' => ['list_id' =>  $request->list_id], 
            'settings' => [
                'subject_line' => $request->subject, 
                'title' => $request->subject, 
                'reply_to' => $request->from_email, 
                'from_name' => $request->from_name] 
            ]);

        $content = Newsletter::getApi()->put("campaigns/".$id."/content", [ 
            'html' => $mailcontent
            ]);
        
        // $response = Newsletter::getApi()->put($newsletterdraft->mailchimp_id, ['html' => $mailcontent ]);
        

        return redirect()->route('admin.newsletters.index');
    }



    public function emailtest($id)
    {
        $newsletterdraft = Newsletterdraft::where('id', $id)->first();

        $client = new \MailchimpMarketing\ApiClient();
        $client->setConfig([
            'apiKey' => env("MAILCHIMP_KEY"),
            'server' => env("MAILCHIMP_Region"),
        ]);

       
        $response = $client->campaigns->test($newsletterdraft->mailchimp_id, [ 'test_emails' => ['damian@beyond-these-walls.co.uk'], "send_type" => "html"  ]);

        return redirect()->route('admin.newsletters.index');
    }

    public function send(Request $request, Newsletter $newsletter)
    {
        // abort_if(Gate::denies('page_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
       
        $template = Template::find(1);

        $replace = ['[[Content]]' => $request->content];

        $string = $template->content;
        $mailcontent = str_replace(array_keys($replace), array_values($replace), $string);

        $newsletter->update($request->all());

        return $mailcontent;

        return redirect()->route('admin.newsletters.index');
    }

    public function destroy($id)
    {
        $response = $client->campaigns->remove($id);

        alert()->success('Imagefile Deleted', 'Success')->autoClose(3000);

        return redirect()->route('admin.newsletters.index');
    }

    public function emailsnippets(MeetupService $meetupservice)
    {
        abort_if(Gate::denies('page_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $json = $meetupservice->events();


        $date = Carbon::now()->addMonths(1)->startOfMonth()->format('Y-m-d');
        $date1 = Carbon::now()->addMonths(2)->startOfMonth()->format('Y-m-d');

        $thismonth = Carbon::now('Europe/Berlin')->format('F');
        $nextmonth =  Carbon::now('Europe/Berlin')->addMonth(1)->format('F');


        $thismonthevents = $json->where('local_date', '<', $date);
        $nextmonthevents = $json->where('local_date', '>=', $date)->where('local_date', '<', $date1);

        
        $events = json_decode($json);
        $events = array_slice($events, 0, 6);

        $thismonthevents = json_decode($thismonthevents);
        $nextmonthevents = json_decode($nextmonthevents);

        return view('admin.newsletters.snippets', compact('events', 'thismonth', 'nextmonth', 'thismonthevents', 'nextmonthevents'));
    }

   

// /2022-05-04

}
