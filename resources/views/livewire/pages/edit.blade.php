<form wire:submit.prevent="submit" class="pt-3">

<div x-data="{name:''}">
  <input id="name" type="text" x-model="name" />
  <p x-text="name">
</div>

    <div class="form-group {{ $errors->has('page.menu_title') ? 'invalid' : '' }}">
        <label class="form-label required" for="menu_title">{{ trans('cruds.page.fields.menu_title') }}</label>
        <input class="form-control" type="text" name="menu_title" id="menu_title" required wire:model.defer="page.menu_title">
        <div class="validation-message">
            {{ $errors->first('page.menu_title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.menu_title_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('page.title') ? 'invalid' : '' }}">
        <label class="form-label required" for="title">{{ trans('cruds.page.fields.title') }}</label>
        <input class="form-control" type="text" name="title" id="title" required wire:model.defer="page.title">
        <div class="validation-message">
            {{ $errors->first('page.title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.title_helper') }}
        </div>
    </div>
<!--     <div class="form-group {{ $errors->has('page.content') ? 'invalid' : '' }}">
        <label class="form-label" for="content">{{ trans('cruds.page.fields.content') }}</label>
        <textarea class="form-control" name="content" id="content" wire:model.defer="page.content" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('page.content') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.content_helper') }}
        </div>
    </div> -->
    <input type="text" id="content" name="content" wire:model.defer="page.content" >
    

    <div class="form-group {{ $errors->has('page.menu') ? 'invalid' : '' }}">
        <label class="form-label" for="menu">{{ trans('cruds.page.fields.menu') }}</label>
        <input class="form-control" type="checkbox" name="menu" id="menu" wire:model.defer="page.menu">
        <div class="validation-message">
            {{ $errors->first('page.menu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.menu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('page.order') ? 'invalid' : '' }}">
        <label class="form-label" for="order">{{ trans('cruds.page.fields.order') }}</label>
        <input class="form-control" type="number" name="order" id="order" wire:model.defer="page.order" step="1">
        <div class="validation-message">
            {{ $errors->first('page.order') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.order_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('page.active') ? 'invalid' : '' }}">
        <label class="form-label" for="active">{{ trans('cruds.page.fields.active') }}</label>
        <input class="form-control" type="checkbox" name="active" id="active" wire:model.defer="page.active">
        <div class="validation-message">
            {{ $errors->first('page.active') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.active_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="mt-3 py-2 px-4 border rounded-md text-sm leading-5 font-medium focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition duration-150 ease-in-out text-white bg-indigo-600 hover:bg-indigo-500 active:bg-indigo-700 border-indigo-600" wire:click="submit()">
            Save
        </button>
        <a href="{{ route('admin.pages.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>