@extends('layouts.admin')
@section('styles')
    <link href="/nlbuilder/newsletterbuilder.css" rel="stylesheet" type="text/css" /> 
@endsection

@section('content')

 <h1 class="text-2xl font-medium text-gray-700">Newsletter: Edit</h1>

 
 <div class="min-h-screen md:flex md:space-x-4">
  
 <div class="flex-1">
    <div class="min-w-full overflow-x-auto shadow sm:rounded-lg bg-white mt-5 mr-5">
        <div class="block text-sm font-medium text-gray-400 px-4 py-3 bg-gray-50 mb-3">Content</div>


        
        <div class="flex justify-center mb-3">



        <div  style="width: 560px">

            <table style="width: 560px">
              <tr>
                <td class="float-center" align="center" valign="top">
                  <center data-parsed="">
                      <table align="center" class="container float-center">
                        <tbody>
                          <tr>
                            <td id="contentarea" class="is-container">
                                {!! $newsletterdraft->content !!}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                  </center>
                </td>
              </tr>
            </table>

        </div>

        </div>

    </div>
  </div>
  <div class="flex-none w-full md:max-w-max ">
    <div class="shadow sm:rounded-lg bg-white px-5 pb-5 mt-5 pt-4">

        <x-form.open action="{{route('admin.newsletters.update', $newsletterdraft->mailchimp_id)}}" id="form">
            @method('PUT')
            @include('admin.newsletters.form')
        </x-form.open> 
        
        <button onclick="save()" class="mt-3 py-2 px-4 border rounded-md text-sm leading-5 font-medium focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition duration-150 ease-in-out text-white bg-custom-600 hover:bg-custom-500 active:bg-custom-700 border-custom-600 block w-full"> Save</button>


    </div>



  </div>
</div>

@endsection

@push('scripts')
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
<script src="/nlbuilder/newsletterbuilder.js" type="text/javascript"></script>
<script src="/assets/email-blocks/content-inlined.js" type="text/javascript"></script>
<script src="/contentbuilder/saveimages.js" type="text/javascript"></script>

<script type="text/javascript">
    var builder = new NewsletterBuilder({
        container: '#contentarea',
        // snippetData: '/assets/email-blocks/snippetlist.html',
        snippetData: '/admin/emailsnippets',
        rowFormat: '<div><table align="center" class="container float-center"><tbody><tr><td><table class="row"><tbody><tr>' +
          '</tr></tbody></table></td></tr></tbody></table></div>',
        cellFormat: '<th class="small-12 large-12 columns first last"><table><tbody><tr><th>' +
          '</th></tr></tbody></table></th>',
        customTags: [["First Name", "{%first_name%}"],
          ["Last Name", "{%last_name%}"],
          ["Email", "{%email%}"]],
        emailMode: true,
        absolutePath: true,
        snippetOpen: false,
        fontAssetPath: '/assets/fonts/',
        buttonsMore: ['icon', 'image', '|', 'list', 'font', 'formatPara']
    });
    
    
    function save() {

        //Save Images
        $("#contentarea").saveimages({
            handler: '/newsletterimage.php',
            onComplete: function () {

            var sHTML = builder.html(); // Get content

            $('#content').val(sHTML);
            $( "#form" ).submit();

            }
        });
        $("#contentarea").data('saveimages').save();
    }


</script>
@endpush
