<?php

namespace App\Http\Livewire\Post;

use App\Models\Post;
use App\Models\User;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Edit extends Component
{
    public Post $post;

    public array $mediaToRemove = [];

    public array $listsForFields = [];

    public array $mediaCollections = [];

    public function mount(Post $post)
    {
        $this->post = $post;
        $this->initListsForFields();
        $this->mediaCollections = [
            'post_image' => $post->image,
        ];
    }

    public function render()
    {
        return view('livewire.post.edit');
    }

    public function submit()
    {
        $this->validate();

        $this->post->save();
        $this->syncMedia();

        return redirect()->route('admin.posts.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    public function getMediaCollection($name)
    {
        return $this->mediaCollections[$name];
    }

    protected function rules(): array
    {
        return [
            'post.user_id' => [
                'integer',
                'exists:users,id',
                'required',
            ],
            'post.title' => [
                'string',
                'required',
            ],
            'post.summary' => [
                'string',
                'required',
            ],
            'post.content' => [
                'string',
                'required',
            ],
            'mediaCollections.post_image' => [
                'array',
                'nullable',
            ],
            'mediaCollections.post_image.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'post.published' => [
                'required',
                'date_format:' . config('project.datetime_format'),
            ],
            'post.featured' => [
                'boolean',
            ],
            'post.active' => [
                'boolean',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['user'] = User::pluck('first_name', 'id');
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->post->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
