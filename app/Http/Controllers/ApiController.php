<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Image;
use Input;
use Response;


class ApiController extends Controller
{


    public function fileupload(Request $request)
    {
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();

        $file->move(public_path().'/uploads/files/', $fileName);

        return Response::json(['name' => $fileName, 'url' => config('app.url').'/uploads/files/'.$fileName]);
    }

    public function upload(Request $request)
    {   

        $types = ['image/png', 'image/jpg', 'image/gif', 'image/jpeg', 'image/pjpeg'];

        if (isset($_FILES['file']))
        {
            foreach ($_FILES['file']['name'] as $key => $name)
            {
                $type = strtolower($_FILES['file']['type'][$key]);
                if (in_array($type, $types))
                {
                    $file = $_FILES['file']['tmp_name'][$key];
                    $filename = time().$_FILES['file']['name'][$key];

                    $thumbnailImage = Image::make($file);
                    $thumbnailPath = public_path().'/uploads/img/thumb/';
                    $originalPath = public_path().'/uploads/img/';

                    $thumbnailImage->save($originalPath.$filename);
                    
                    $thumbnailImage->resize(150,150);
                    $thumbnailImage->save($thumbnailPath.$filename);

                }
            }
        }

         return response()->json([
            'file' => [
                'id' => '1',
                'url' => config('app.url').'/uploads/img/'.$filename,
            ]
        ],200);
    }

    public function imagefeed()
    {

        $filesInFolder = \File::files('uploads/img/thumb');
        $myfiles = [];

        foreach ($filesInFolder as $path) {
            $file = pathinfo($path);
            $myfiles[] = ['thumb' => config('app.url').'/uploads/img/thumb/'.$file['basename'], 'url' => '/uploads/img/'.$file['basename'], 'id' => $file['filename'], 'title' => $file['filename']];
        }
        echo json_encode($myfiles);
    }

    public function filefeed()
    {
        $filesInFolder = \File::files('uploads/files');
        $myfiles = [];

        foreach ($filesInFolder as $path) {
            $file = pathinfo($path);
            $myfiles[] = ['url' => config('app.url').'/uploads/files/'.$file['basename'], 'title' => $file['filename'], 'name' => $file['filename'], 'id' => $file['filename']];
        }
        echo json_encode($myfiles);
    }
}