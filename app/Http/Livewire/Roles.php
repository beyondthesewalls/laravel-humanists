<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Carbon;
use App\Http\Livewire\DataTable\WithSorting;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use Illuminate\Support\Str;

class Roles extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public $showDeleteModal = false;
    public $showDeleteSingleModal = false;
    public $deleteID = null;

    public $search;

    public $showEditModal = false;
    public $showFilters = false;
    public $myroles = '';

    public array $permissions = [];

    public $filters = [
        'search' => ''
    ];
    public Role $editing;

    
    protected $queryString = ['sorts'];

    public array $listsForFields = [];

    protected $listeners = ['refreshTransactions' => '$refresh'];

    public function rules() { return [
        'editing.title' => 'required|min:3'
    ]; }

    public function clearSearch(): void
     {
         $this->search = '';
     }

    public function mount() { 
        $this->editing = $this->makeBlankTransaction(); 
    }
    public function updatedFilters() { $this->resetPage(); }

    public function exportSelected()
    {
        return response()->streamDownload(function () {
            echo $this->selectedRowsQuery->toCsv();
        }, 'transactions.csv');
    }

    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->delete();

        $this->showDeleteModal = false;

        $this->notify('You\'ve deleted '.$deleteCount.' roles');
    }

    public function deleteSingle()
    {
        $this->showDeleteSingleModal = false;

        $transaction = Role::findOrFail($this->deleteID)->delete();

        $this->notify('You\'ve deleted a role');
    }

    public function makeBlankTransaction()
    {
        return Role::make();
    }

    public function toggleShowFilters()
    {
        $this->useCachedRows();

        $this->showFilters = ! $this->showFilters;
    }

    public function create()
    {
        $this->useCachedRows();
        $this->permissions = [];

        if ($this->editing->getKey()) $this->editing = $this->makeBlankTransaction();

        $this->showEditModal = true;
    }

    public function edit(Role $role)
    {
        $this->useCachedRows();

        $this->editing = $role;
        $this->permissions = $role->permissions()->pluck('id')->toArray();
 
        $this->showEditModal = true;
    }

    public function save()
    {
        $this->validate();

        $this->editing->save();
        $this->editing->permissions()->sync($this->permissions);

        $this->showEditModal = false;
    }

    public function resetFilters() { $this->reset('filters'); }

    public function getRowsQueryProperty()
    {
        $query = Role::query()
            ->when($this->filters['search'], fn($query, $status) => $query->where('title', $status));

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        $this->myroles = Permission::all();

        return view('livewire.roles', [
            'roles' => $this->rows,
        ]);
    }

    public function delete($id)
    {
        $this->useCachedRows();

        $this->deleteID = $id;

        $this->showDeleteSingleModal = true;
    }



}
