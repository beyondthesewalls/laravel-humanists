@props([
    'method' => 'post',
    'action' => ''
])

<form method="{{ $method === 'get' ? 'get' : 'post' }}" action="{{ $action }}" {{ $attributes}}>
@if ($method != 'get')
    @csrf
@endif

@if (! in_array(strtoupper($method), ['get', 'post']))
    @method($method)
@endif

{{ $slot }}

</form>