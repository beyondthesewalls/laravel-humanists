<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <script>

        var data_basic = {
            'snippets': [
                {
                    'thumbnail': '/assets/minimalist-blocks/preview/hero-02.png',
                    'category': '125',
                    'html':
                        '<section class="block py-24 leading-7 text-left text-gray-900">' +
                              '<div class="relative w-full px-5 px-8 mx-auto leading-7 text-gray-900 max-w-7xl lg:px-16">' +
                                  '<div class="flex flex-col flex-wrap items-center text-left md:flex-row">' +
                                      '<div class="flex-1 opacity-100 xl:pr-12 transform-none">' +
                                          '<h1 class="box-border mt-0 text-4xl font-bold tracking-tight text-center text-gray-900 sm:text-5xl md:text-4xl lg:text-5xl mb-7 md:text-left">Hello!</span>' +
                                          '</h1>' +                   
                                          '<p class="box-border mt-0 mb-8 text-base font-normal text-center text-gray-500 lg:text-xl md:text-left lg:mb-8">Central London Humanists is the local humanist group for anyone who lives, works or otherwise finds themselves in central London, or anyone from further away who wants to come along.<BR><BR> The group currently meets regularly online but we are planning more in-person events in the coming months.</p>' +
                                      '</div>' +
                                      '<div class="relative flex justify-center flex-1 w-full px-5 mt-16 leading-7 text-gray-900 md:justify-end md:mt-0">' +
                                          '<img src="/assets/images/phone1.png" class="w-full max-w-md">' +
                                      '</div>' +
                                  '</div>' +
                              '</div>' +
                          '</section>'
                },

                {
                'thumbnail': '/assets/minimalist-blocks/preview/meetup-01.png',
                'category': '130',
                'html':
                        '<section class="py-20 " x-data="meetupData()" x-init="getEvents()">' +
                            '<div class="container max-w-6xl mx-auto mb-4">' +
                                '<h2 class="text-4xl font-bold tracking-tight text-center">Upcoming Events and Socials</h2>' +
                                '<p class="mt-2 text-lg text-center text-gray-600">Every month we hold events such as talk/lectures and several social events. Our social activities include group socials, a book group, picnics, walks, museum & cultural visits and theatre trips. All our events are arranged on <a class="text-blue-600" href="https://www.meetup.com/Central-London-Humanists/events">Meetup</a>.</p>' +
                            '</div>' +

                            '<div class="container max-w-7xl mx-auto">' +
                                '<div class="text-center font-light text-xl" x-text="title"></div>' +
                                 '<div class="flex flex-wrap gallery-style-4" data-protected>' +
                                    '<template x-for="event in events" :key="event.id">' +                                    
                                      '<div class="lg:w-1/3 sm:w-1/2 p-4">' +
                                        '<div class="flex relative">' +
                                          '<a x-bind:href="event.link" target="_blank">' +
                                            '<div class="single-gallery">' +
                                              '<div class="image">' +
                                                  '<template x-if="event.featured_photo">' +
                                                    '<img :src="event.featured_photo.highres_link" class="bg-white rounded-md border-gray-300" alt="">' +
                                                  '</template>' +
                                                  '<template x-if="!event.featured_photo">' +
                                                    '<img src="/assets/images/holding.png" class="bg-white rounded-md border border-gray-300" alt="">' +
                                                 '</template>' +
                                                '<div class="overlay">' +
                                                  '<div class="action">' +
                                                  '<svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20 text-white" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" /></svg>' +
                                                  '</div>' +
                                                '</div>' +
                                             '</div>' +
                                              '<div class="info rounded-md">' +
                                                '<template x-if="event.venue">' +
                                                  '<div class="text-sm font-extralight" x-text="event.venue.name"></div>' +
                                                '</template>' +
                                               '<div class="title text-xl mb-2 font-medium" x-text="event.name"></div>' +
                                                 '<div class="relative text-md font-semibold pb-5">' +
                                                  '<div class="absolute right-0 top-0" x-text="event.local_time"></div>' +
                                                  '<div class="text-md font-semibold "x-text="(new Date(event.local_date).toLocaleString(\'en-UK\', {weekday: \'long\', day: \'numeric\', year: \'numeric\',  month: \'long\'}))"></div>' +
                                                '</div>' +
                                              '</div>' +
                                            '</div>' +
                                            '</a>' +
                                       '</div>' +
                                      '</div>' +
                                    '</template>' +
                              '</div>' +
                            '</div>' +
                        '</section>'
                }, 


                {
                    'thumbnail': '/assets/minimalist-blocks/preview/meetup-01.png',
                    'category': '120',
                    'html':
                        '<section class="text-gray-600 body-font gallery-section gallery-style-4 mb-80">' +
                            '<div class="flex flex-col text-center w-full mb-10">' +
                                  '<h1 class="sm:text-3xl lg:text-4xl font-bold title-font mb-4 text-gray-900">Upcoming Events and Meetings</h1>' +
                                  '<p class="lg:w-2/3 mx-auto leading-relaxed text-base">Whatever cardigan tote bag tumblr hexagon brooklyn asymmetrical gentrify, jianbing selfies heirloom.</p>' +
                            '</div>' +

                            '<div class="flex flex-wrap gallery-style-4">' +
                     
                                @foreach ($events as $event)
                                '<div class="lg:w-1/3 sm:w-1/2 p-4">' +
                                    '<div class="flex relative">' +
                                        '<a href="{{$event->link}}" target="_blank">' +
                                            '<div class="single-gallery">' +
                                                '<div class="image">' +
                                                    @if (!empty($event->featured_photo))
                                                    '<img src="{{$event->featured_photo->photo_link}}" alt="">' +
                                                    @else
                                                    '<img src="/assets/images/3_Characters.png" class="bg-white" alt="">' +
                                                    @endif
                                                    '<div class="overlay">' +
                                                        '<div class="action">' +
                                                            '<i class="fal fa-external-link-alt fa-3x text-white"></i>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '</div>' +

                                                '<div class="info">' +
                                                    '<div class="title text-2xl mb-4 font-medium">{{$event->name}}</div>' +
                                                    '<div>{!!date('jS F', strtotime($event->local_date))!!} - {!!date('g:ia', strtotime($event->local_time))!!} </div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</a>' +
                                     '</div>' +    
                                '</div>' +
                                @endforeach

                            '</div>' +
                        '</section>'
                },

                {
                    'thumbnail': '/assets/minimalist-blocks/preview/hero-02.png',
                    'category': '101',
                    'html':
                        '<section class="py-20 bg-white" x-data="postData()" x-init="getPosts()">' +
                          '<div class="container max-w-6xl mx-auto">' +
                            '<div class="mb-5">' +
                              '<h2 class="text-4xl font-bold tracking-tight text-center">Our Features</h2>' +
                                '<p class="mt-2 text-lg text-center text-gray-600">Check out our list of awesome features below.</p>' +
                                
                            '</div>' +

                            '<div class="flex flex-col sm:flex-row justify-between mx-4 md:mx-0 lg:-mx-2 flex-wrap" data-protected>' +
                                '<template x-for="post in posts" :key="post.id">  ' +
                                  '<a :href="\'/posts/\' + post.slug"  class="rounded overflow-hidden shadow-md flex-1 bg-white sm:mx-2 md:mx-1 lg:mx-2 w-full sm:w-1/3 lg:pt-0 mb-10 flex flex-col text-gray-900 hover:text-custom-500 transition duration-300 ease-in-out hover:shadow-lg">' +
                                    '<template x-if="post.image">' +
                                      '<img :src="\'/uploads/posts/thumb/\' + post.image" class="w-full object-cover h-32 sm:h-48 md:h-48 border" :alt="post.title">' +
                                    '</template>' +
                                    '<template x-if="!post.image">' +
                                      '<img src="/assets/images/3_Characters.png" class="w-full object-cover h-32 sm:h-48 md:h-48 border" alt="Default Image">' +
                                   '</template>' +

                                    '<div class="p-4 md:p-6 bg-white flex flex-col flex-1">' +
                                      '<span class="flex-1">' +
                                        '<h3 class="title text-xl mb-2 font-medium leading-tight sm:leading-normal" x-text="post.title"></h3>' +
                                      '</span>' +
                                      '<div class="text-sm flex items-center">' +
                                        '<div class="flex items-center mt-4">' +
                                            '<div class="flex flex-col justify-between text-sm">' +
                                                '<p class="text-gray-800 dark:text-white" >' +
                                                    '<span x-text="post.user.name"></span> <span x-text="post.user.last_name"></span>' +
                                                '</p>' +
                                                '<p class="text-gray-400 dark:text-gray-300">' +
                                                    '<span x-text="post.display_date"></span> - <span x-text="post.readtime"></span> min read' +
                                                '</p>' +
                                            '</div>' +
                                        '</div>' +
                                      '</div>' +
                                    '</div>' +
                                  '</a>' +
                                '</template>' +
                              '</div>' +
                          '</div>' +
                        '</section>'
                },

                {
                    'thumbnail': '/assets/minimalist-blocks/preview/hero-02.png',
                    'category': '101',
                    'html':
                        '<section class="py-20 bg-white" x-data="postData()" x-init="getPosts()">' +
                          '<div class="container max-w-6xl mx-auto">' +
                            '<div class="mb-5">' +
                              '<h2 class="text-4xl font-bold tracking-tight text-center">Our Features</h2>' +
                                '<p class="mt-2 text-lg text-center text-gray-600">Check out our list of awesome features below.</p>' +
                            '</div>' +

                            '<div class="text-center font-light text-xl" x-text="title"></div>' +
                            '<div class="flex flex-wrap gallery-style-4" data-protected>' +

                              '<template x-for="post in posts" :key="post.id">' +                                    
                                        '<div class="lg:w-1/3 xs:w-1/2 p-3">' +
                                          '<div class="flex relative">' +
                                            '<a :href="\'/posts/\' + post.slug"  class="rounded overflow-hidden shadow-md flex-1 bg-white sm:mx-2 md:mx-1 lg:mx-2 w-full sm:w-1/3 lg:pt-0 mb-10 flex flex-col text-gray-900 hover:text-custom-500 transition duration-300 ease-in-out hover:shadow-lg">' +

                                              '<template x-if="post.image">' +
                                                '<img :src="\'/uploads/posts/thumb/\' + post.image" class="w-full object-cover h-32 sm:h-48 md:h-48 border" :alt="post.title">' +
                                              '</template>' +
                                              '<template x-if="!post.image">' +
                                                '<img src="/assets/images/3_Characters.png" class="w-full object-cover h-32 sm:h-48 md:h-48 border" alt="Default Image">' +
                                              '</template>' +

                                              '<div class="p-4 md:p-6 bg-white flex flex-col flex-1">' +
                                                '<span class="flex-1">' +
                                                  '<h3 class="title text-xl mb-2 font-medium leading-tight sm:leading-normal" x-text="post.title"></h3>' +
                                                '</span>' +
                                                '<div class="text-sm flex items-center">' +
                                                  '<div class="flex items-center mt-4">' +
                                                      '<div class="flex flex-col justify-between text-sm">' +
                                                          '<p class="text-gray-800 dark:text-white" >' +
                                                              '<span x-text="post.user.name"></span> <span x-text="post.user.last_name"></span>' +
                                                          '</p>' +
                                                          '<p class="text-gray-400 dark:text-gray-300">' +
                                                              '<span x-text="post.display_date"></span> - <span x-text="post.readtime"></span> min read' +
                                                          '</p>' +
                                                      '</div>' +
                                                  '</div>' +
                                                '</div>' +
                                              '</div>' +


                                            '</a>' +
                                          '</div>' +
                                       '</div>' +
                              '</template>' +
                            '</div>' +
                          '</div>' +
                        '</section>'
                },
        

                {
                    'thumbnail': '/assets/minimalist-blocks/preview/basic-06.png',
                    'category': '120',
                    'html':
                    '<div class="container px-4 py-5">' +
                        '<div class="row flex-lg-row-reverse align-items-center g-5 ">' +
                            '<div class="col-10 col-sm-8 col-lg-5">' +
                                '<img src="/assets/images/Humaaans - Phone.png" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">' +
                            '</div>' +
                            '<div class="col-lg-7">' +
                                '<h2 class="fw-bold lh-1 mb-3">Responsive left-aligned hero with image</h2>' +
                                '<p class="lead">Quickly design and customize responsive mobile-first sites with Bootstrap, the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins.</p>' +
                                '<div class="mt-3">' +
                                    '<a href="#" class="is-btn is-btn-ghost2 is-upper is-btn-small">Buy Now</a>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                },
              


                {
                    'thumbnail': '/assets/minimalist-blocks/preview/basic-01.png',
                    'category': '120',
                    'html':
                        '<div class="container max-w-lg px-4 py-32 mx-auto text-left md:max-w-none md:text-center">' +
                        '<div class="bg-white dark:bg-gray-800 overflow-hidden relative">' +
                            '<div class="text-start w-1/2 py-12 px-4 sm:px-6 lg:py-16 lg:px-8 z-20">' +
                                '<h2 class="text-3xl font-extrabold text-black dark:text-white sm:text-4xl">' +
                                    '<span class="block">' +
                                        'Want to be millionaire ?' +
                                   '</span>' +
                                    '<span class="block text-indigo-500">' +
                                        'It&#x27;s today or never.' +
                                    '</span>' +
                                '</h2>' +
                                '<div class="lg:mt-0 lg:flex-shrink-0">' +
                                    '<div class="mt-12 inline-flex rounded-md shadow">' +
                                        '<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small">Buy Now</a>'+
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                           '<img src="/uploads/018.jpg" class="absolute h-full max-w-1/2 hidden lg:block right-0 top-0"/>' +
                        '</div>' +
                        '</div>'
                },

                {
                    'thumbnail': '/assets/minimalist-blocks/preview/basic-01.png',
                    'category': '120',
                    'html':
                        '<div class="row clearfix">' +
                            '<div class="column full">' +
                                '<div class="display">' +
                                '<h1>Beautiful Content. Responsive.</h1>' +
                                '<p><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</i></p>' +
                                '</div>' +
                            '</div>' +
                        '</div>'
                },

                {
                    'thumbnail': '/assets/minimalist-blocks/preview/basic-01.png',
                    'category': '120',
                    'html':

                           
                                '<div class="row">' +
                                    @foreach ($events as $event)
                                    '<div class="col-lg-4 col-md-6">' +
                                        '<a href="{{$event->link}}" target="_blank">' +
                                            '<div class="single-gallery">' +
                                                '<div class="image">' +
                                                    @if (!empty($event->featured_photo))
                                                    '<img src="{{$event->featured_photo->photo_link}}" alt="">' +
                                                    @else
                                                    '<img src="/assets/images/3_Characters.png" class="bg-white" alt="">' +
                                                    @endif
                                                    '<div class="overlay">' +
                                                        '<div class="action">' +
                                                            '<i class="fal fa-external-link-alt fa-3x text-white"></i>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '</div>' +

                                                '<div class="info">' +
                                                    '<h5 class="title">{{$event->name}}</h5>' +
                                                    '<div>{!!date('jS F', strtotime($event->local_date))!!} - {!!date('g:ia', strtotime($event->local_time))!!} </div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</a>' +
                                    '</div>' +
                                    @endforeach
                                '</div>'
                       
                },

                

                {
                    'thumbnail': '/assets/minimalist-blocks/preview/basic-01.png',
                    'category': '120',
                    'html':
                        '<section class="gallery-section gallery-style-4 mb-80">' +
                            '<div class="container">' +
                                '<div class="row">' +
                                    @foreach ($nextmonth as $event)
                                    '<div class="col-lg-4 col-md-6">' +
                                        '<a href="{{$event->link}}" target="_blank">' +
                                            '<div class="single-gallery">' +
                                                '<div class="image">' +
                                                    @if (!empty($event->featured_photo))
                                                    '<img src="{{$event->featured_photo->photo_link}}" alt="">' +
                                                    @else
                                                    '<img src="/assets/images/3_Characters.png" class="bg-white" alt="">' +
                                                    @endif
                                                    '<div class="overlay">' +
                                                        '<div class="action">' +
                                                            '<i class="fal fa-external-link-alt fa-3x text-white"></i>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '</div>' +

                                                '<div class="info">' +
                                                    '<h5 class="title">{{$event->name}}</h5>' +
                                                    '<div>{!!date('jS F', strtotime($event->local_date))!!} - {!!date('g:ia', strtotime($event->local_time))!!} </div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</a>' +
                                    '</div>' +
                                    @endforeach
                                '</div>' +
                            '</div>' +
                        '</section>'
                }
            ]

        };


    </script>
    <style>
        body {
            background: #fff;
            margin: 0;
        }

        .is-design-list {
            position: fixed;
            top: 0px;
            left: 0px;
            border-top: transparent 80px solid;
            width: 100%;
            height: 100%;
            overflow-y: auto;
            padding: 0px 0px 30px 30px;
            box-sizing: border-box;
            overflow: auto;
        }

        .is-design-list>div {
            width: 250px;
            overflow: hidden;
            background: #000;
            margin: 32px 40px 0 0;
            cursor: pointer;
            display: inline-block;
            outline: #dbdbdb 1px solid;
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.05);
        }

        .is-design-list>div img {
            box-shadow: none;
            opacity: 1;
            display: block;
            box-sizing: border-box;
            transition: all 0.2s ease-in-out;
            max-width: 400px;
            width: 100%
        }

        .is-design-list>div:hover img {
            opacity: 0.95;
        }

        .is-category-list {
            position: relative;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 80px;
            box-sizing: border-box;
            z-index: 1;
        }

        .is-category-list>div {
            white-space: nowrap;
            padding: 0 30px;
            box-sizing: border-box;
            font-family: sans-serif;
            font-size: 10px;
            text-transform: uppercase;
            letter-spacing: 2px;
            background: #f5f5f5;
        }

        .is-category-list a {
            display: inline-block;
            padding: 10px 20px;
            background: #fefefe;
            color: #000;
            border-radius: 50px;

            margin: 0 12px 0 0;
            text-decoration: none;
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.03);
            transition: box-shadow ease 0.3s;
        }

        .is-category-list a:hover {
            /*background: #fafafa;*/
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.06);
            color: #000;
        }

        .is-category-list a.active {
            background: #f5f5f5;
            color: #000;
            box-shadow: none;
            cursor: default;
        }

        .is-more-categories {
            display: none;
            position: absolute;
            width: 400px;
            box-sizing: border-box;
            padding: 0;
            z-index: 1;
            font-family: sans-serif;
            font-size: 10px;
            text-transform: uppercase;
            letter-spacing: 2px;
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.05);
            background: #fff;
        }

        .is-more-categories a {
            width: 200px;
            float: left;
            display: block;
            box-sizing: border-box;
            padding: 12px 20px;
            background: #fff;
            text-decoration: none;
            color: #000;
            line-height: 1.6;
        }

        .is-more-categories a:hover {
            background: #eee;
        }

        .is-more-categories a.active {
            background: #eee;
        }

        .is-more-categories.active {
            display: block;
        }

        /* First Loading */
        /* .is-category-list {
            display: none;
        }

        .is-design-list {
            display: none;
        }

        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        .pace-inactive {
            display: none;
        }

        .pace .pace-progress {
            background: #000000;
            position: fixed;
            z-index: 2000;
            top: 0;
            right: 100%;
            width: 100%;
            height: 2px;
        } */

        .is-more-categories>a:nth-child(0) {
            display: none
        }

        .is-more-categories>a:nth-child(1) {
            display: none
        }

        .is-more-categories>a:nth-child(2) {
            display: none
        }

        .is-more-categories>a:nth-child(3) {
            display: none
        }

        .is-more-categories>a:nth-child(4) {
            display: none
        }

        .is-more-categories>a:nth-child(5) {
            display: none
        }

        .is-more-categories>a:nth-child(6) {
            display: none
        }

        .is-more-categories>a:nth-child(7) {
            display: none
        }

        @media all and (max-width: 1212px) {
            .is-categories>a:nth-child(7):not(.more-snippets) {
                display: none
            }

            .is-more-categories>a:nth-child(7) {
                display: block
            }
        }

        @media all and (max-width: 1070px) {
            .is-categories>a:nth-child(6):not(.more-snippets) {
                display: none
            }

            .is-more-categories>a:nth-child(6) {
                display: block
            }
        }

        @media all and (max-width: 940px) {
            .is-categories>a:nth-child(5):not(.more-snippets) {
                display: none
            }

            .is-more-categories>a:nth-child(5) {
                display: block
            }
        }

        @media all and (max-width: 700px) {
            .is-categories>a:nth-child(4):not(.more-snippets) {
                display: none
            }

            .is-more-categories>a:nth-child(4) {
                display: block
            }
        }

        @media all and (max-width: 555px) {
            .is-categories>a:nth-child(3):not(.more-snippets) {
                display: none
            }

            .is-more-categories>a:nth-child(3) {
                display: block
            }
        }

        @media all and (max-width: 415px) {
            .is-categories>a:nth-child(2):not(.more-snippets) {
                display: none
            }

            .is-more-categories>a:nth-child(2) {
                display: block
            }
        }

        @media all and (max-width: 640px) {
            .is-more-categories a {
                width: 150px;
                padding: 10px 5px 10px 15px;
                font-size: 10px;
            }

            .is-more-categories {
                left: 0 !important;
                width: 100% !important;
            }
        }
    </style>
</head>

<body>
    <svg style="display:none">
        <defs>
            <symbol viewBox="0 0 512 512" id="ion-ios-close-empty">
                <path d="M340.2 160l-84.4 84.3-84-83.9-11.8 11.8 84 83.8-84 83.9 11.8 11.7 84-83.8 84.4 84.2 11.8-11.7-84.4-84.3 84.4-84.2z"></path>
            </symbol>
        </defs>
    </svg>

    <div class="is-pop-close"
        style="z-index:10;width:30px;height:30px;position:absolute;top:0px;right:0px;box-sizing:border-box;padding:0;line-height:40px;font-size: 12px;color:#777;text-align:center;cursor:pointer;">
        <svg class="is-icon-flex" style="fill:rgba(0, 0, 0, 0.47);width:30px;height:30px;">
            <use xlink:href="#ion-ios-close-empty"></use>
        </svg>
    </div>

    <div class="is-category-list">
        <div class="is-categories" style="position:fixed;top:0;left:0;right:0;height:68px;padding-top:17px;box-sizing:border-box;">
            <!-- <a href="" data-cat="120" class="active">Core</a>
            <a href="" data-cat="118">Meetup</a>
            <a href="" data-cat="101">Headline</a>
            <a href="" data-cat="119">Buttons</a>
            <a href="" data-cat="102">Photos</a>
            <a href="" data-cat="103">Profile</a>
            <a href="" data-cat="116">Contact</a>
            <a href="" class="more-snippets">More</a> -->
        </div>
    </div>
    <div class="is-more-categories">
        <!-- <a href="" data-cat="120" class="active">Core</a>
        <a href="" data-cat="118">Article</a>
        <a href="" data-cat="101">Headline</a>
        <a href="" data-cat="119">Buttons</a>
        <a href="" data-cat="102">Photos</a>
        <a href="" data-cat="103">Profile</a>
        <a href="" data-cat="116">Contact</a>
        <a href="" data-cat="104">Products</a>
        <a href="" data-cat="105">Features</a>
        <a href="" data-cat="106">Process</a>
        <a href="" data-cat="107">Pricing</a>
        <a href="" data-cat="108">Skills</a>
        <a href="" data-cat="109">Achievements</a>
        <a href="" data-cat="110">Quotes</a>
        <a href="" data-cat="111">Partners</a>
        <a href="" data-cat="112">As Featured On</a>
        <a href="" data-cat="113">Page Not Found</a>
        <a href="" data-cat="114">Coming Soon</a>
        <a href="" data-cat="115">Help, FAQ</a> -->
    </div>

    <div class="is-design-list">
    </div>

    <script>

        var snippetPath = parent._cb.opts.snippetPath;
        var snippetCategories = parent._cb.opts.snippetCategories;
        var defaultSnippetCategory = parent._cb.opts.defaultSnippetCategory;

        var numOfCat = snippetCategories.length;
        if (numOfCat <= 7) {
            document.querySelector('.is-more-categories').style.width = '200px';
        }

        var categorytabs = document.querySelector('.is-categories');
        categorytabs.innerHTML = '';
        let html_catselect = '';
        for (var i = 0; i < numOfCat; i++) {
            if (i < 7) {
                html_catselect += '<a href="" data-cat="' + snippetCategories[i][0] + '">' + snippetCategories[i][1] + '</a>';
            }
        }
        html_catselect += '<a href="" class="more-snippets">' + parent._cb.out('More') + '</a>';
        categorytabs.innerHTML = html_catselect;

        var categorymore = document.querySelector('.is-more-categories');
        html_catselect = '';
        for (var i = 0; i < numOfCat; i++) {
            html_catselect += '<a href="" data-cat="' + snippetCategories[i][0] + '">' + snippetCategories[i][1] + '</a>';
        }
        categorymore.innerHTML = html_catselect;

        // Show/hide "More" button
        if (numOfCat <= 7) {
            var bHasMore = false;

            const childNodes = categorymore.childNodes;
            let i = childNodes.length;
            while (i--) {
                if(childNodes[i].style.display === 'block') {
                    bHasMore = true;
                }
            }
            var more = document.querySelector('.more-snippets');
            if (!bHasMore) more.style.display = 'none';
            else more.style.display = '';
        }

        /*
        jQuery(window).on('resize', function (e) {
            var bHasMore = false;
            jQuery('.is-more-categories').children().each(function () {
                if (jQuery(this).css('display') == 'block') {
                    bHasMore = true;
                }
            });
            if (!bHasMore) jQuery('.more-snippets').css('display', 'none');
            else jQuery('.more-snippets').css('display', '');
        });*/

        var elms = categorytabs.querySelectorAll('a[data-cat="' + defaultSnippetCategory + '"]'); //.classList.add('active');
        Array.prototype.forEach.call(elms, function(elm){
            elm.classList.add('active');
        });
        elms = categorymore.querySelectorAll('a[data-cat="' + defaultSnippetCategory + '"]'); //.classList.add('active');
        Array.prototype.forEach.call(elms, function(elm){
            elm.classList.add('active');
        });

        var snippets = data_basic.snippets; //DATA

        // Hide slider snippet if slick is not included
        var bHideSliderSnippet = true;
        if(parent.jQuery) {
            if(parent.jQuery.fn.slick) {
                bHideSliderSnippet = false;
            }
        }
        for (var nIndex = 0; nIndex < data_basic.snippets.length; nIndex++) {
            if (data_basic.snippets[nIndex].thumbnail.indexOf('element-slider.png') != -1 && bHideSliderSnippet) {
                data_basic.snippets.splice(nIndex, 1);
                break;
            }
        }
        
        var designlist = document.querySelector('.is-design-list');
        for (let i = 0; i <snippets.length; i++) {
            
            snippets[i].id = i+1;
            var thumb = snippets[i].thumbnail;

            thumb = snippetPath + thumb;

            if (snippets[i].category === defaultSnippetCategory + '') {
                designlist.insertAdjacentHTML('beforeend', '<div data-id="' + snippets[i].id + '" data-cat="' + snippets[i].category + '"><img src="' + thumb + '"></div>');
            
                var newitem = designlist.querySelector('[data-id="' + snippets[i].id + '"]');
                newitem.addEventListener('click', function(e){

                    var snippetid = e.target.parentNode.getAttribute('data-id');
                    addSnippet(snippetid);

                });

            }

        }

        var categorylist = document.querySelector('.is-category-list');
        elms = categorylist.querySelectorAll('a');
        Array.prototype.forEach.call(elms, function(elm){

            elm.addEventListener('click', function(e){

                if(elm.classList.contains('active')) return false;

                var cat = elm.getAttribute('data-cat');
                if(designlist.querySelectorAll('[data-cat="' + cat + '"]').length === 0) {

                    for (let i = 0; i <snippets.length; i++) {
                
                        var thumb = snippets[i].thumbnail;
                        
                        thumb = snippetPath + thumb;    

                        if (snippets[i].category === cat) {
                            designlist.insertAdjacentHTML('beforeend', '<div data-id="' + snippets[i].id + '" data-cat="' + snippets[i].category + '"><img src="' + thumb + '"></div>');
                        
                            var newitem = designlist.querySelector('[data-id="' + snippets[i].id + '"]');
                            newitem.addEventListener('click', function(e){
                                
                                var snippetid = e.target.parentNode.getAttribute('data-id');
                                addSnippet(snippetid);

                            });
                        }

                    }    
                }

                if (cat) {
                    // Hide all, show items from selected category
                    var categorylist_items = categorylist.querySelectorAll('a');    
                    Array.prototype.forEach.call(categorylist_items, function(elm){
                        elm.className = elm.className.replace('active', '');
                    });
                    categorymore.className = categorymore.className.replace('active', ''); 
                    var categorymore_items = categorymore.querySelectorAll('a');
                    Array.prototype.forEach.call(categorymore_items, function(elm){
                        elm.className = elm.className.replace('active', '');
                    });

                    var items = designlist.querySelectorAll('div');
                    Array.prototype.forEach.call(items, function(elm){
                        elm.style.display = 'none';
                    });
                    Array.prototype.forEach.call(items, function(elm){
                        var catSplit = elm.getAttribute('data-cat').split(',');
                        for (var j = 0; j < catSplit.length; j++) {
                            if (catSplit[j] == cat) {
                                elm.style.display = ''; // TODO: hide & show snippets => animated
                            }
                        }
                    });
                    
                } else {
                    // more snipptes
                    var more = document.querySelector('.more-snippets');
                    var moreCategories = document.querySelector('.is-more-categories');
                    if(more.classList.contains('active')) {
                        more.className = more.className.replace('active', '');
                        moreCategories.className = moreCategories.className.replace('active', '');
                    } else {
                        var _width = moreCategories.offsetWidth;
                        more.classList.add('active');
                        moreCategories.classList.add('active');
                        var top = more.getBoundingClientRect().top;
                        var left = more.getBoundingClientRect().left;
                        top = top + 50;
                        moreCategories.style.top = top + 'px';
                        moreCategories.style.left = left + 'px';
                    }
                }
                elm.classList.add('active');

                e.preventDefault();
            });

        });

        elms = categorymore.querySelectorAll('a');
        Array.prototype.forEach.call(elms, function(elm){

            elm.addEventListener('click', function(e){
                
                var cat = elm.getAttribute('data-cat');
                if(designlist.querySelectorAll('[data-cat="' + cat + '"]').length === 0) {

                    for (let i = 0; i <snippets.length; i++) {
                
                        var thumb = snippets[i].thumbnail;
                        
                        thumb = snippetPath + thumb;

                        if (snippets[i].category === cat) {
              
                            designlist.insertAdjacentHTML('beforeend', '<div data-id="' + snippets[i].id + '" data-cat="' + snippets[i].category + '"><img src="' + thumb + '"></div>');
                        
                            var newitem = designlist.querySelector('[data-id="' + snippets[i].id + '"]');
                            newitem.addEventListener('click', function(e){
                                
                                var snippetid = e.target.parentNode.getAttribute('data-id');
                                addSnippet(snippetid);

                            });
                        }

                    }    
                }

                // Hide all, show items from selected category
                Array.prototype.forEach.call(elms, function(elm){
                    elm.className = elm.className.replace('active', '');
                });
                categorymore.className = categorymore.className.replace('active', ''); // hide popup
                //var categorymore_items = categorymore.querySelectorAll('a');
                
                var categorylist = document.querySelector('.is-category-list');
                var categorylist_items = categorylist.querySelectorAll('a');                
                Array.prototype.forEach.call(categorylist_items, function(elm){
                    elm.className = elm.className.replace('active', '');
                });
                    
                var more = document.querySelector('.more-snippets');
                more.className = more.className.replace('active', '');

                var items = designlist.querySelectorAll('div');
                Array.prototype.forEach.call(items, function(elm){
                    elm.style.display = 'none';
                });
                Array.prototype.forEach.call(items, function(elm){
                    var catSplit = elm.getAttribute('data-cat').split(',');
                    for (var j = 0; j < catSplit.length; j++) {
                        if (catSplit[j] == cat) {
                            elm.style.display = '';
                        }
                    }
                });

                elm.classList.add('active');

                e.preventDefault();
            });

        });

        
        var close = document.querySelector('.is-pop-close');
        close.addEventListener('click', function(e){
            var modal = parent.document.querySelector('.is-modal.snippets');
            removeClass(modal, 'active');
        });

        // Add document Click event
        document.addEventListener('click', function(e){
            e = e || window.event;
            var target = e.target || e.srcElement;  

            if(parentsHasClass(target, 'more-snippets')) return;
            if(hasClass(target, 'more-snippets')) return;
            
            var more = document.querySelector('.more-snippets');
            var moreCategories = document.querySelector('.is-more-categories');
            
            more.className = more.className.replace('active', '');
            moreCategories.className = moreCategories.className.replace('active', '');
        });

        parent.document.addEventListener('click', function(e){
            var more = document.querySelector('.more-snippets');
            var moreCategories = document.querySelector('.is-more-categories');
            
            more.className = more.className.replace('active', '');
            moreCategories.className = moreCategories.className.replace('active', '');
        });

        function addSnippet(snippetid) {
            
            // TODO: var framework = parent._cb.opts.framework;
            var snippetPathReplace = parent._cb.opts.snippetPathReplace;
            var emailMode = parent._cb.opts.emailMode;

            // 
            for (let i = 0; i <snippets.length; i++) {
                if(snippets[i].id + ''=== snippetid) {
                    
                    var html = snippets[i].html;
                    var noedit = snippets[i].noedit;
                    break;
                }
            }

            var bSnippet;
            if (html.indexOf('row clearfix') === -1) {
                bSnippet = true; // Just snippet (without row/column grid)
            } else {
                bSnippet = false; // Snippet is wrapped in row/colum
            }
            if (emailMode) bSnippet = false;
 
            // Convert snippet into your defined 12 columns grid   
            var rowClass = parent._cb.opts.row; //row
            var colClass = parent._cb.opts.cols; //['col s1', 'col s2', 'col s3', 'col s4', 'col s5', 'col s6', 'col s7', 'col s8', 'col s9', 'col s10', 'col s11', 'col s12']
            if(rowClass!=='' && colClass.length===12){
                html = html.replace(new RegExp('row clearfix', 'g'), rowClass);
                html = html.replace(new RegExp('column full', 'g'), colClass[11]);
                html = html.replace(new RegExp('column half', 'g'), colClass[5]);
                html = html.replace(new RegExp('column third', 'g'), colClass[3]);
                html = html.replace(new RegExp('column fourth', 'g'), colClass[2]);
                html = html.replace(new RegExp('column fifth', 'g'), colClass[1]);
                html = html.replace(new RegExp('column sixth', 'g'), colClass[1]);
                html = html.replace(new RegExp('column two-third', 'g'), colClass[7]);
                html = html.replace(new RegExp('column two-fourth', 'g'), colClass[8]);
                html = html.replace(new RegExp('column two-fifth', 'g'), colClass[9]);
                html = html.replace(new RegExp('column two-sixth', 'g'), colClass[9]);
            }
            
            html = html.replace(/{id}/g, makeid()); // Replace {id} with auto generated id (for custom code snippet)

            if(parent._cb.opts.onAdd){
                html = parent._cb.opts.onAdd(html);
            }
            
            if(snippetPathReplace.length>0) {
                // try {
                    if (snippetPathReplace[0] != '') {
                        var regex = new RegExp(snippetPathReplace[0], 'g');
                        html = html.replace(regex, snippetPathReplace[1]);

                        var string1 = snippetPathReplace[0].replace(/\//g, '%2F');
                        var string2 = snippetPathReplace[1].replace(/\//g, '%2F');

                        var regex2 = new RegExp(string1, 'g');
                        html = html.replace(regex2, string2);
                    }
                // } catch (e) { }
            }
            
            parent._cb.addSnippet(html, bSnippet, noedit);

            var modal = parent.document.querySelector('.is-modal.snippets');
            removeClass(modal, 'active');

        }

        function hasClass(element, classname) {
            if(!element) return false;
            return element.classList ? element.classList.contains(classname) : new RegExp('\\b'+ classname+'\\b').test(element.className);
        }

        function removeClass(element, classname) {
            if(!element) return;
            if(element.classList.length>0) {
                element.className = element.className.replace(classname, '');
            }
        }
        
        function parentsHasClass(element, classname) {
            while (element) {
                if(element.tagName === 'BODY' || element.tagName === 'HTML') return false;
                if(!element.classList) return false;
                if (element.classList.contains(classname)) {
                    return true;
                }
                element = element.parentNode;
            }
        }

        function makeid() {//http://stackoverflow.com/questions/1349404/generate-a-string-of-5-random-characters-in-javascript
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            for (var i = 0; i < 2; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            var text2 = "";
            var possible2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 5; i++)
                text2 += possible2.charAt(Math.floor(Math.random() * possible2.length));

            return text + text2;
        }

       
    </script>

</textarea>
</body>

</html>