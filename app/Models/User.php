<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function scopeFindUserByName($query,$name) {
    // Concat the name columns and then apply search query on full name
        $query->where(DB::raw(
                // REPLACE will remove the double white space with single (As defined)
                "REPLACE(
                    /* CONCAT will concat the columns with defined separator */
                    CONCAT(
                        /* COALESCE operator will handle NUll values as defined value. */
                        COALESCE(name,''),' ',
                        COALESCE(last_name,'')
                    ),
                '  ',' ')"
            ),
        'like', '%' . $name . '%');
    }

    public function getFullName()
    {
        return $this->name.' '.$this->last_name;
    }

    public function getFullNameAttribute($value)
    {
        return $this->name.' '.$this->last_name;
    }


    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
