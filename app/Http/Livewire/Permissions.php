<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Carbon;
use App\Http\Livewire\DataTable\WithSorting;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use Illuminate\Support\Str;

class Permissions extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public $showDeleteModal = false;
    public $showDeleteSingleModal = false;
    public $deleteID = null;
    public $search;

    public $showEditModal = false;
    public $showFilters = false;
    public $filters = [
        'search' => ''
    ];
    public Permission $editing;

    
    protected $queryString = ['sorts'];

    protected $listeners = ['refreshTransactions' => '$refresh'];

    public function rules() { return [
        'editing.title' => 'required|min:3'
  
    ]; }

    public function clearSearch(): void
     {
         $this->search = '';
     }

    public function mount() { 
        $this->editing = $this->makeBlankTransaction(); 
    }
    public function updatedFilters() { $this->resetPage(); }

    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->delete();

        $this->showDeleteModal = false;

        $this->notify('You\'ve deleted '.$deleteCount.' transactions');
    }

    public function deleteSingle()
    {
        $this->showDeleteSingleModal = false;

        $transaction = Permission::findOrFail($this->deleteID)->delete();

        $this->notify('You\'ve deleted a permission');
    }

    public function makeBlankTransaction()
    {
        return Permission::make();
    }

    public function toggleShowFilters()
    {
        $this->useCachedRows();

        $this->showFilters = ! $this->showFilters;
    }

    public function create()
    {
        $this->useCachedRows();

        if ($this->editing->getKey()) $this->editing = $this->makeBlankTransaction();

        $this->showEditModal = true;
    }

    public function edit(Permission $permission)
    {
        $this->useCachedRows();

        $this->editing = $permission;
 
        $this->showEditModal = true;
    }

    public function save()
    {
        $this->validate();

        $this->editing->save();

        $this->showEditModal = false;
    }

    public function resetFilters() { $this->reset('filters'); }

    public function getRowsQueryProperty()
    {
        $query = Permission::query()
            // ->when($this->filters['name'], fn($query, $status) => $query->where('name', $status))
            ->when($this->filters['search'], fn($query, $search) => $query->where('title', 'like', '%'.$search.'%'));

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        return view('livewire.permissions', [
            'permissions' => $this->rows,
        ]);
    }

    public function delete($id)
    {
        $this->useCachedRows();

        $this->deleteID = $id;

        $this->showDeleteSingleModal = true;
    }



}
