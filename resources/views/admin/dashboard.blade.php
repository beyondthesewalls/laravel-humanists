@extends('layouts.admin')
@section('content')


<div class="mx-auto w-full">

    <h1 class="text-2xl font-semibold text-gray-900 mb-5">Dashboard</h1>

        <div>
            <!-- Card stats -->
            <div class="flex flex-wrap -mx-4">
                <div class="w-full md:w-1/3 px-4">
                    <div class="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg">
                        <div class="flex-auto p-4">
                            <div class="flex flex-wrap">
                                <div class="relative w-full pr-4 max-w-full flex-grow flex-1">
                                    <h5 class="text-gray-500 uppercase font-bold text-xs">
                                        Meetup Members
                                    </h5>
                                    <span class="font-semibold text-xl text-gray-800">
                          000
                        </span>
                                </div>
                                <div class="relative w-auto px-2 flex-initial">
                                    <div
                                        class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-red-500">
                                        <svg class="w-6 h-6" fill="currentColor" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 32 32" ><path d="M 9.5 1 A 0.5 0.5 0 0 0 9.5 2 A 0.5 0.5 0 0 0 9.5 1 z M 18.5 2 A 1.5 1.5 0 0 0 18.5 5 A 1.5 1.5 0 0 0 18.5 2 z M 14 5 C 11.383 5 9.16375 6.678625 8.34375 9.015625 C 5.93275 9.099625 4 11.068 4 13.5 C 4 14.207 4.17775 14.867891 4.46875 15.462891 C 3.56175 16.367891 3 17.618 3 19 C 3 21.422 4.7227656 23.441391 7.0097656 23.900391 C 7.0087656 23.934391 7 23.966 7 24 C 7 26.761 9.239 29 12 29 C 13.213 29 14.309781 28.551031 15.175781 27.832031 C 15.899781 28.554031 16.898 29 18 29 C 19.868 29 21.424281 27.713422 21.863281 25.982422 C 24.730281 25.793422 27 23.415 27 20.5 C 27 19.348 26.645062 18.279484 26.039062 17.396484 C 26.631062 16.769484 27 15.93 27 15 C 27 13.599 26.171422 12.399844 24.982422 11.839844 C 24.989422 11.726844 25 11.615 25 11.5 C 25 8.462 22.538 6 19.5 6 C 18.877 6 18.280656 6.1078281 17.722656 6.2988281 C 16.699656 5.4878281 15.407 5 14 5 z M 7 6 A 1 1 0 0 0 7 8 A 1 1 0 0 0 7 6 z M 27 9 A 1 1 0 0 0 27 11 A 1 1 0 0 0 27 9 z M 17.005859 9.9921875 C 17.152094 9.9790625 17.314656 9.9865781 17.503906 10.017578 C 18.014906 10.091578 18.311219 10.441547 18.699219 10.810547 C 19.013219 11.130547 19.240922 10.933125 19.419922 10.828125 C 19.696922 10.668125 19.913281 10.527875 20.738281 10.546875 C 21.600281 10.564875 22.585922 10.879422 22.794922 12.357422 C 23.022922 14.014422 20.140078 18.269234 20.330078 20.240234 C 20.465078 21.626234 22.794375 20.639203 22.984375 21.908203 C 23.232375 23.557203 20.085813 22.942797 19.382812 22.591797 C 18.255813 22.024797 17.566609 20.742609 17.849609 19.474609 C 18.083609 18.526609 20.158141 14.653219 20.244141 14.074219 C 20.318141 13.458219 19.999594 13.402344 19.808594 13.402344 C 19.543594 13.384344 19.352891 13.508484 19.087891 13.896484 C 18.859891 14.247484 16.271922 19.512219 16.044922 19.949219 C 15.323922 21.341219 13.457969 21.113125 13.667969 19.703125 C 13.722969 19.284125 15.379266 15.516922 15.447266 14.919922 C 15.502266 14.568922 15.430125 14.179281 15.078125 13.988281 C 14.727125 13.809281 14.302688 14.093734 14.179688 14.302734 C 14.000687 14.604734 11.678641 20.782281 11.431641 21.238281 C 10.994641 22.026281 10.550187 22.273547 9.8671875 22.310547 C 8.2661875 22.402547 7.0667031 21.046203 7.5957031 19.408203 C 7.8237031 18.669203 9.3681094 13.114125 10.162109 11.703125 C 10.692109 10.755125 12.15225 10.035656 13.15625 10.472656 C 13.68625 10.700656 14.425766 11.068828 14.634766 11.173828 C 15.127766 11.407828 15.655234 10.791281 15.865234 10.613281 C 16.276234 10.257781 16.567156 10.031562 17.005859 9.9921875 z M 30.5 13 A 0.5 0.5 0 0 0 30.5 14 A 0.5 0.5 0 0 0 30.5 13 z M 1.5 14 A 1.5 1.5 0 0 0 1.5 17 A 1.5 1.5 0 0 0 1.5 14 z M 29 16 A 1 1 0 0 0 29 18 A 1 1 0 0 0 29 16 z M 5.5 25 A 0.5 0.5 0 0 0 5.5 26 A 0.5 0.5 0 0 0 5.5 25 z M 23.5 27 A 1.5 1.5 0 0 0 23.5 30 A 1.5 1.5 0 0 0 23.5 27 z M 15 29 A 1 1 0 0 0 15 31 A 1 1 0 0 0 15 29 z"/></svg>
                                    </div>
                                </div>
                            </div>
                            <p class="text-sm text-gray-500 mt-4">
                      <span class="text-red-500 mr-2">
                        <i class="fas fa-arrow-down"></i> 0%
                      </span>
                                <span class="whitespace-no-wrap">
                        Since last week
                      </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="w-full md:w-1/3 px-4">
                    <div class="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg">
                        <div class="flex-auto p-4">
                            <div class="flex flex-wrap">
                                <div class="relative w-full pr-4 max-w-full flex-grow flex-1">
                                    <h5 class="text-gray-500 uppercase font-bold text-xs">
                                        Email Subscribers
                                    </h5>
                                    <span class="font-semibold text-xl text-gray-800">
                          000
                        </span>
                                </div>
                                <div class="relative w-auto px-2 flex-initial">
                                    <div
                                        class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-pink-500">
                                        <svg class="w-5 h-5" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                             stroke-width="2" stroke="currentColor" viewBox="0 0 24 24">
                                            <path
                                                d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <p class="text-sm text-gray-500 mt-4">
                      <span class="text-orange-500 mr-2">
                        <i class="fas fa-arrow-down"></i> 0%
                      </span>
                                <span class="whitespace-no-wrap">
                        Since yesterday
                      </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="w-full md:w-1/3 px-4">
                    <div class="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg">
                        <div class="flex-auto p-4">
                            <div class="flex flex-wrap">
                                <div class="relative w-full pr-4 max-w-full flex-grow flex-1">
                                    <h5 class="text-gray-500 uppercase font-bold text-xs">
                                        Members
                                    </h5>
                                    <span class="font-semibold text-xl text-gray-800">
                         000
                        </span>
                                </div>
                                <div class="relative w-auto px-2 flex-initial">
                                    <div
                                        class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-blue-500">
                                        <svg class="w-5 h-5" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                             stroke-width="2" stroke="currentColor" viewBox="0 0 24 24">
                                            <path
                                                d="M9.75 17L9 20l-1 1h8l-1-1-.75-3M3 13h18M5 17h14a2 2 0 002-2V5a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <p class="text-sm text-gray-500 mt-4">
                      <span class="text-green-500 mr-2">
                        <i class="fas fa-arrow-up"></i> 0%
                      </span>
                                <span class="whitespace-no-wrap">
                        Since last month
                      </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    






@endsection