@extends('layouts.admin')
@section('styles')
    <link rel="stylesheet" href="/js/redactor/redactor.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')

 <h1 class="text-2xl font-medium text-gray-700">Posts: Edit</h1>

 <x-form.open action="{{route('admin.posts.update', $post->id)}}" id="form" enctype="multipart/form-data">
        @method('PUT')
 <div class="min-h-screen md:flex md:space-x-4">
  
  <div class="flex-1">
    <div class="min-w-full overflow-x-auto shadow sm:rounded-lg bg-white mt-5 mr-5">
      <div class="block text-sm font-medium text-gray-400 px-4 py-3 bg-gray-50">Summary</div>
      <textarea name="summary" id="summary" class="redactor ">{{ $post->summary }}</textarea>

    </div>

  	<div class="min-w-full overflow-x-auto shadow sm:rounded-lg bg-white mt-5 mr-5">
    	<div class="block text-sm font-medium text-gray-400 px-4 py-3 bg-gray-50">Content</div>
  		<textarea name="content" id="content" class="redactor ">{{ $post->content }}</textarea>

    </div>
  </div>
  <div class="flex-none w-full md:max-w-max ">
    <div class="shadow sm:rounded-lg bg-white px-5 pb-5 mt-5 pt-4">
    	
	

	        @include('admin.posts.form')

	    
    </div>
  </div>
</div>
</x-form.open>
@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<script src="/js/redactor/redactor.js"></script>
<script src="/js/redactor/plugins/imagemanager.js"></script>
<script src="/js/redactor/plugins/table.js"></script>
<script src="/js/redactor/plugins/alignment.js"></script>
<script src="/js/redactor/plugins/fontcolor.js"></script>
<script src="/js/redactor/plugins/counter.js"></script>
<script src="/js/redactor/plugins/fullscreen.js"></script>
<script src="/js/redactor/plugins/inlinestyle.js"></script>
<script src="/js/redactor/plugins/specialchars.js"></script>
<script src="/js/redactor/plugins/video.js"></script>
<script src="/js/redactor/plugins/fullscreen.js"></script>




<script>
  flatpickr(".flatpickr", {
    enableTime: true,
    dateFormat: "d-m-Y H:i",
  });

  $R('.redactor', { 
    buttons: ['html', 'format', 'redo', 'undo', 'format', 'bold', 'italic', 'lists', 'image', 'link'],
    plugins:['imagemanager', 'table', 'alignment', 'fontcolor', 'counter', 'definedlinks','fullscreen', 'inlinestyle', 'specialchars', 'video'],
    imageUpload: '/upload/images?_token=' + '{{ csrf_token() }}',
    imageManagerJson: '/images/feed',
    imageResizable: true,
    imagePosition: true,
    multipleUpload: false,
    imageFigure: false,


  });


    $('.redactor1').redactor({
            buttons: ['source','format', 'text', 'redo', 'undo', 'bold', 'italic', 'lists', 'image', 'file', 'video', 'link', 'table', 'fullscreen'],
            plugins:['fullscreen', 'fontcolor', 'bufferbuttons', 'alignment','imagemanager','filemanager','table', 'source', 'video'],
            imageUpload: '/upload/images?_token=' + '{{ csrf_token() }}',
            imageManagerJson: '/images/feed',
            fileUpload: '/upload/files',
            fileManagerJson: '/file/feed',

            toolbarOverflow: true,
            minHeight: 200,
            source: true,
            imageResizable: true,
            imagePosition: true,
            multipleUpload: false,
            imageFigure: false,
            callbacks: {
                image: {
                    uploadError: function(response)
                    {
                        console.log(response.message);
                    }
                },
                file: {
                    uploadError: function(response)
                    {
                        console.log(response.message);
                    }
                }
            }
        });

</script>
@endpush