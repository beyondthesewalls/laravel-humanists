@extends('layouts.front')
@section('content')
<section class="box-border pt-5 leading-7 text-gray-900 bg-white border-0 border-gray-200 border-solid pb-7 ">
	<div class="box-border mx-auto border-solid max-w-7xl px-1">

		<h1 class="text-4xl font-bold text-gray-900 text-4xl mb-7 md:text-left" >{!! $category->title !!}</h1>
		<div class="grid grid-cols-1 md:grid-cols-8 gap-4">
			<!-- Main -->
		  	<div class="md:col-span-5 lg:col-span-6">

				<div class="md:flex content-center flex-wrap" x-data="postData()" x-init="getPosts()">

					@foreach ($posts as $post) 
						  <div class="md:flex lg:w-1/2 px-2 mb-5 pb-4">
					        <a href="/posts/{!! $post->slug !!}" class="md:flex-1  rounded-md shadow-md bg-white  overflow-hidden text-gray-900 hover:text-custom-500 transition duration-300 ease-in-out hover:shadow-lg">

					              <img src="/uploads/posts/{{ $post->image }}" class="w-full object-cover h-32 sm:h-48 md:h-48" alt="Default Image">

					          <div class="p-4 md:p-6 bg-white flex flex-col flex-1 border">
					              <span class="flex-1">
					                <h3 class="title text-xl mb-2 font-medium leading-tight sm:leading-normal">{!! $post->title !!}</h3>
					              </span>
					              <div class="text-sm flex items-center">
					                <div class="flex items-center mt-4">
					                    <div class="flex flex-col justify-between text-sm">
					                        <p class="text-gray-800 dark:text-white" >
					                            {{ $post->user->full_name }}
					                        </p>
					                        <p class="text-gray-400 dark:text-gray-300">
					                            {{ $post->display_date }} - {{ $post->readtime }} min read
					                        </p>
					                    </div>
					                </div>
					              </div>
					            </div>
					        </a>
					      </div>
					  @endforeach

			    </div>

			</div>

			<!-- Col -->
			<div class="md:col-span-3 lg:col-span-2">

				
			  	
			</div>
		</div>

	</div>
</section>
@endsection