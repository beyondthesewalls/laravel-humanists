@extends('layouts.front')
@section('content')




<section class="box-border pt-5 leading-7 text-gray-900 bg-white border-0 border-gray-200 border-solid pb-7 ">
	<div class="box-border px-2 mx-auto border-solid max-w-7xl">

		<img src="/uploads/posts/{{ $post->image }}" class="w-full mb-5 rounded-md shadow-md" alt="Default Image">

		<h3 class="title text-3xl mb-2 font-medium">{!! $post->title !!}</h3>

		<p class="text-gray-800 dark:text-white" >
            {{ $post->user->full_name }}
        </p>
        <p class="text-gray-400 dark:text-gray-300">
            {{ $post->display_date }} - {{ $post->readtime }} min read
        </p>

         <div  class="leading-7 mt-10 space-y-6">
        	{!! $post->content !!}
        </div>
		





@endsection