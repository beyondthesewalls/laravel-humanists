@extends('layouts.front')
@section('content')
<section class="box-border pt-5 leading-7 text-gray-900 bg-white border-0 border-gray-200 border-solid pb-7 ">
	<div class="box-border px-1 mx-auto border-solid max-w-7xl">


		<div class="grid grid-cols-1 md:grid-cols-8 gap-4">
			<!-- Main -->
		  	<div class="md:col-span-5 lg:col-span-6">

				<div class="md:flex content-center flex-wrap" x-data="postData()" x-init="getPosts()">

					@isset ($featured)
				      <div class="md:flex px-2 mb-5 pb-4">
				        <a href="/posts/{!! $featured->slug !!}" class="md:flex-1  rounded-md shadow-md bg-white  overflow-hidden text-gray-900 hover:text-custom-500 transition duration-300 ease-in-out hover:shadow-lg">

				              <img src="/uploads/posts/{{ $featured->image }}" class="w-full object-cover h-72" alt="Default Image">

				          <div class="p-4 md:p-6 bg-white flex flex-col flex-1 border">
				              <span class="flex-1">
				                <h3 class="title text-2xl mb-2 font-medium leading-tight sm:leading-normal">{!! $featured->title !!}</h3>
				                <p  class="font-base">
				                	{!! $featured->summary !!}
				                </p>
				              </span>
				              <div class="text-sm flex items-center">
				                <div class="flex items-center mt-4">
				                    <div class="flex flex-col justify-between text-sm">
				                        <p class="text-gray-800 dark:text-white" >
				                            {{ $featured->user->full_name }}
				                        </p>
				                        <p class="text-gray-400 dark:text-gray-300">
				                            {{ $featured->published }} - {{ $featured->readtime }} min read
				                        </p>
				                    </div>
				                </div>
				              </div>
				            </div>
				        </a>
				      </div>
				      @endisset

				      @foreach ($posts as $post) 
						  <div class="md:flex lg:w-1/2 px-2 mb-5 pb-4">
					        <a href="/posts/{!! $post->slug !!}" class="md:flex-1  rounded-md shadow-md bg-white  overflow-hidden text-gray-900 hover:text-custom-500 transition duration-300 ease-in-out hover:shadow-lg">

					              <img src="/uploads/posts/{{ $post->image }}" class="w-full object-cover h-32 sm:h-48 md:h-48" alt="Default Image">

					          <div class="p-4 md:p-6 bg-white flex flex-col flex-1 border">
					              <span class="flex-1">
					                <h3 class="title text-xl mb-2 font-medium leading-tight sm:leading-normal">{!! $post->title !!}</h3>
					              </span>
					              <div class="text-sm flex items-center">
					                <div class="flex items-center mt-4">
					                    <div class="flex flex-col justify-between text-sm">
					                        <p class="text-gray-800 dark:text-white" >
					                            {{ $post->user->full_name }}
					                        </p>
					                        <p class="text-gray-400 dark:text-gray-300">
					                            {{ $post->display_date }} - {{ $post->readtime }} min read
					                        </p>
					                    </div>
					                </div>
					              </div>
					            </div>
					        </a>
					      </div>
					  @endforeach

			    </div>

			</div>

			<!-- Col -->
			<div class="md:col-span-3 lg:col-span-2">

				<div class="rounded-md shadow-md border p-3">
                    <h2 class="mb-5 text-lg font-medium text-gray-900">Categories</h2>
                    <ul>
                    	@foreach ($categories as $category) 
                        	<li class="flex"><a href="/posts/category/{!! $category->slug !!}" class="flex-1 block py-2 text-md font-thin text-gray-900 hover:text-custom-500">{{ $category->title }}</a><span class="p-2 text-md font-thin text-gray-700">{{ $category->posts->count() }}</span></li>
                        @endforeach
                    </ul>
                </div>
                <div class="rounded-md shadow-md border p-3 mt-5">
                    <h2 class="mb-5 text-lg font-medium text-gray-900">Recent Posts</h2>
                    <ul>
                    	@foreach ($newposts as $post) 
                        <li class="mb-5">
                            <a href="/posts/{!! $post->slug !!}" class="flex">
                                <div class="w-1/3 overflow-hidden rounded pt-1">
                                    <img class="object-cover object-center h-20 rounded" src="/uploads/posts/thumb/{{ $post->image }}" alt="">
                                </div>
                                <div class="flex flex-col items-start justify-center w-2/3 px-2">
                                    <h3 class="mb-2 font-thin text-gray-900">{!! $post->title !!}</h3>
                                    <span class="block text-xs font-thin text-gray-800">{{ $post->display_date }}</span>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
			  	
			</div>
		</div>

	</div>
</section>
@endsection