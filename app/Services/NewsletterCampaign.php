<?php

namespace App\Services;

use Illuminate\Http\Request;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
// use App\Models\Newsletter;

use Newsletter;

class NewsletterCampaign extends Newsletter
{

    public function createCampaigns(
        string $fromName,
        string $replyTo,
        string $subject,
        string $html = '',
        string $listName = '',
        array $options = [],
        array $contentOptions = []
    ) {
        $list = $this->lists->findByName($listName);

        $defaultOptions = [
            'type' => 'regular',
            'recipients' => [
                'list_id' => $list->getId(),
                'segment_opts' => array(
                        'saved_segment_id' => 1952965,
                    ),
            ],
            'settings' => [
                'subject_line' => $subject,
                'from_name' => $fromName,
                'reply_to' => $replyTo,
            ],
        ];

        $options = array_merge($defaultOptions, $options);

        $response = $this->mailChimp->post('campaigns', $options);

        if (! $this->lastActionSucceeded()) {
            return false;
        }

        if ($html === '') {
            return $response;
        }

        if (! $this->updateContent($response['id'], $html, $contentOptions)) {
            return false;
        }

        return $response;
    }

}