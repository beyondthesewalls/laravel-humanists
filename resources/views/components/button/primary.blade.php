<x-button {{ $attributes->merge(['class' => 'text-white bg-custom-600 hover:bg-custom-500 active:bg-custom-700 border-custom-600']) }}>{{ $slot }}</x-button>
