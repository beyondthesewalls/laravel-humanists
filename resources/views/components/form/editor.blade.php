@if ($label != '')
	<label for='{{ $name }}' class="block text-sm font-medium text-gray-400">{{ $label }}</label>
@endif

<div class="mb-4">
    <textarea {!! $attributes->merge([
            'class' => 'mb-4' . ($label ? ' mt-1' : '')
        ]) !!}>{{ $slot }}</textarea>
</div>
