<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\ContentController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\NewsletterController;

use App\Http\Controllers\MailChimpController;

use App\Http\Controllers\ApiController;

use App\Http\Livewire\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Redactor Image Upload
Route::post('/upload/images', [ApiController::class, 'upload']);
Route::get('/images/feed', [ApiController::class, 'imagefeed']);



//Redactor File Upload
// Route::post('upload/files', 'ApiController@fileupload');
// Route::get('file/feed', 'ApiController@filefeed');



Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth:sanctum', 'verified']], function () {

    

    Route::get('/', [HomeController::class, 'index'])->name('home');

    // Pages
	Route::resource('pages', PageController::class, ['except' => ['destroy', 'show']]);
	Route::get('snippets', [PageController::class, 'snippets'])->name('snippets');


	// Permissions
    Route::resource('permissions', PermissionController::class, ['except' => ['store', 'update', 'destroy']]);

    // Roles
    Route::resource('roles', RoleController::class, ['except' => ['store', 'update', 'destroy']]);

    // Users
    Route::resource('users', UserController::class, ['except' => ['store', 'update', 'destroy']]);

    Route::resource('posts', PostController::class, ['except' => ['destroy', 'show']]);

    // Newsletters
    Route::resource('newsletters', NewsletterController::class);
    Route::get('newsletters/{id}/test', [NewsletterController::class, 'gettest'])->name('emailtest');
    Route::post('newsletters/{id}/test', [NewsletterController::class, 'sendtest']);
    Route::get('newsletters/{id}/send', [NewsletterController::class, 'send'])->name('emailsend');

    Route::get('emailsnippets', [NewsletterController::class, 'emailsnippets'])->name('emailsnippets');

});

// Route::middleware(['auth:sanctum', 'verified'])->get('/admin', function () {
//     return view('dashboard');
// })->name('admin');
Route::get('/send-mail-using-mailchimp', [MailChimpController::class, 'index'])->name('send.mail.using.mailchimp.index');



//Posts
Route::get('/posts', [ContentController::class, 'posts'])->name('posts');
Route::get('/posts/{id}', [ContentController::class, 'post'])->name('post');
Route::get('/posts/category/{id}', [ContentController::class, 'category'])->name('category');


// Users
Route::resource('users', UserController::class);

// API
Route::get('/api/meetup', [ContentController::class, 'api_meetup'])->name('api_meetup');
Route::get('/api/posts', [ContentController::class, 'api_posts'])->name('api_posts');


Route::get('/test', function() {
  return File::get(public_path() . '/classy-mailchimp.html');
});

// Home

Route::resource('/signup', MailChimpController::class);
Route::get('/signup/{id}/remove', [MailChimpController::class, 'remove'])->name('remove');


Route::name('home')->get('/', [ContentController::class, 'index'])->name('index');
Route::get('/home', [ContentController::class, 'index'])->name('index');
Route::get('/{page}', [ContentController::class, 'page'])->name('page');