<div
    wire:ignore
    x-data="{ values: @entangle($attributes->wire('model')), choices: null }"
    x-init="
        choices = new Choices($refs.multiple, {
            itemSelectText: '',
            removeItems: true,
            removeItemButton: true,
        });

        for (const [value, label] of Object.entries(values)) {
            choices.setChoiceByValue(value || label)
        }

        $refs.multiple.addEventListener('change', function (event) {
            values = []
            Array.from($refs.multiple.options).forEach(function (option) {
                values.push(option.value || option.text)
            })
        })
    "
>
    <select x-ref="multiple" multiple="multiple">
        <option value="1">asdasd</option>
	    	<option value="2">asdasfsdfd</option>
	    	<option value="3">adsdfsdasd</option>
    </select>
</div>