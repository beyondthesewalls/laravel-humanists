<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
// use App\Models\Newsletter;

use Newsletter;

class MailChimpController extends Controller
{

    public function index()
    {
        $response = Newsletter::getApi()->get('lists/a007760c2a/interest-categories/f7ce0f15a0/interests/');    
        // return $response;

        $options = [];
        $sent = 0;
        $hasmember = 0;

        return view('front.pages.signup', compact('options', 'sent', 'hasmember'));
    }

    public function store(Request $request)
    {
        $interest1 = false;
        $interest2 = false;
        $interest3 = false;

        if ( $request->has('interest1') ) {
            $interest1 = true;
        }
        if ( $request->has('interest2') ) {
            $interest2 = true;
        }
        if ( $request->has('interest3') ) {
            $interest3 = true;
        }

        $hasmember = Newsletter::hasMember($request->EMAIL);

        $response = Newsletter::subscribeOrUpdate($request->EMAIL, ['FNAME'=>$request->FNAME,'LNAME'=>$request->LNAME], 'subscribers', ['interests'=>['410d53c68d'=>$interest1, 'c9fedc5641'=>$interest2, 'c90bc770f9'=>$interest3]]);

        $sent = 1;

        return view('front.pages.signup', compact('sent', 'hasmember'));
    }


    public function show($id)
    {

        // $member = Newsletter::getMember('damian.belson@gmail.com');

        // $response = $client->lists->getListMember("list_id", "subscriber_hash");

        // $response = Newsletter::getApi()->get('lists/a007760c2a/members? ='.$id.'&fields=members.email_address');  

        // $response = $response = Arr::get($response, 'members');


        // $response = Newsletter::getApi()->get('lists/a007760c2a/members/'.$id);  

        $response = Newsletter::getApi()->get('lists/a007760c2a/members?unique_email_id='.$id.'&fields=members.email_address');  

        $member_email =  Arr::flatten($response)[0];

        $member = collect(Newsletter::getMember($member_email));

        $sent = 0;
        $hasmember = 0;

        // return $member;


        return view('front.pages.update', compact('member', 'sent', 'hasmember'));

    }

    public function remove($id)
    {
        Newsletter::getApi()->delete('lists/a007760c2a/members/'.$id);  

        notify()->success('Your email has been removed');

        return redirect()->route('index');
    }


    public function index2()
    {

        $response = Newsletter::subscribeOrUpdate('jimbob@beyond-these-walls.co.uk', ['FNAME'=>'Rince','LNAME'=>'Wind'], 'subscribers', ['interests'=>['410d53c68d'=>true, 'c9fedc5641'=>false]]);


        // $response = Mailchimp::getLists();
        // return $response;

        // $email = 'daimo@beyond-these-walls.co.uk';
        // $first = 'Damino';
        // $last = 'Tester';

        // $member = (new \NZTim\Mailchimp\Member($email))->merge_fields(['FNAME' => $first, 'LNAME' => $last])->interests(['410d53c68d' => true, 'c9fedc5641' => true])->confirm(false);
        // Mailchimp::addUpdateMember('a007760c2a', $member);

        // $response = Mailchimp::api('get','/search-members',['query'=> 'daimo@beyond-these-walls.co.uk', 'list_id' => 'a007760c2a'] );
        // $response = Mailchimp::api('get','/campaigns');
        // $response = Mailchimp::api('post','/campaigns', [
        //     'type' => 'regular',
        //     'list_id' => 'a007760c2a',
        //     'subject_line' => 'August Newsletter',
        //     'reply_to' => 'damian@beyond-these-walls.co.uk',
        //     'from_email' => 'damian@beyond-these-walls.co.uk',
        //     'from_name' => 'Damian'
        //     ],
        //     'settings', [  
        //         'title' => 'New Newsletter',
        //         'to_name' => 'FNAME'
        //     ],
        //     'recipients', [ 
        //         'list_id' => 'a007760c2a',
        //     ],
        //     [
        //         'html' => 'hello *|FNAME|*',
        //         'text' => 'hello'
        //     ]
        // );
       


        // $campaign = $mailchimp->campaigns->create('regular', [
        //     'list_id' => 'a007760c2a',
        //     'subject' => 'New Newsletter',
        //     'from_email' => 'clh@gmail.com',
        //     'from_name' => 'Damian',
        //     'to_name' => 'Subscribers'

        //     ],
        //     [
        //         'html' => 'hello *|FNAME|*',
        //         'text' => 'hello'
        //     ],
        //     [
        //         'saved_segment_id' => '1923309',
        //         'match' => 'any'
        //     ]
        // );


        return $response;










        // $client = new \MailchimpMarketing\ApiClient();

        // $client->setConfig([
        //   'apiKey' => 'c83e916712d151c5993c27d18ed7f922',
        //   'server' => 'us6'

        // ]);

        // $response = $client->campaigns->remove('77a1ffb145');
        // $response = $client->lists->listSegments('00c0445b5b');
        // $response = $client->campaigns->getContent('abd8cf326d');
        // $response = $client->campaigns->get('0d1fd7d3a1');

        // $response = $client->lists->getListMembersInfo('a007760c2a');

        // $response = $client->root->getRoot();

        // $response = $client->lists->addListMember('a007760c2a', [
        //     "email_address" => "david@beyond-these-walls.co.uk",
        //     "status" => "subscribed",
        // ]);

        // $response = $client->lists->addListMember('a007760c2a', [
        //     'email' => 'bert@beyond-these-walls.co.uk',
        //     'status' => 'subscribed',
        //     'merge_fields'  => [
        //         'FIRSTNAME' => 'Bert',
        //         'LASTNAME' => 'Smith'
        //      ],
        //     'groups'  => [
        //         '410d53c68d' => true,
        //         'c9fedc5641' => true,
        //         'c90bc770f9' => true
        //      ]
        //  ]

        // );


        // $response = $client->campaigns->list();

        // $response = $client->lists->addListMember("887ffe1925", [
        //     "email_address" => "Milo31@gmail.com",
        //     "status" => "subscribed",
        // ]);

        // $response = $client->lists->addListMember('887ffe1925', [
        //     'email_address' => 'daimob@btinternet.com',
        //     'status' => 'subscribed'
        //     ]
        // );



        // $collection = collect($response);

        // $first = $collection->first();

        // $collection = $collection->flatten()->where('id', '0d1fd7d3a1');

        // $collection =  $collection->values()->all();

        // return $collection;

    }






    public function index1(Request $request)
    {
        $listId = '887ffe1925';

        $mailchimp = new \Mailchimp('c83e916712d151c5993c27d18ed7f922-us6');

        // $campaign = $mailchimp->lists->getList();

        $campaign = $mailchimp->lists->subscribe('a007760c2a', [
            'email' => 'sasha@beyond-these-walls.co.uk',
            'status' => 'subscribed',
            'merge_fields'  => [
                'FIRSTNAME' => 'Sarah',
                'LASTNAME' => 'Bobb'
             ],
            'groups'  => [
                '410d53c68d' => true,
                'c9fedc5641' => true,
                'c90bc770f9' => true
             ]
            
            ]
        );


        // $campaign = $mailchimp->campaigns->create('regular', [
        //     'list_id' => '00c0445b5b',
        //     'subject' => 'New Newsletter',
        //     'from_email' => 'clh@gmail.com',
        //     'from_name' => 'Damian',
        //     'to_name' => 'Subscribers'

        //     ],
        //     [
        //         'html' => 'hello *|FNAME|*',
        //         'text' => 'hello'
        //     ],
        //     [
        //         'saved_segment_id' => '1923309',
        //         'match' => 'any'
        //     ]
        // );

        //Send campaign
        // $mailchimp->campaigns->send($campaign['id']);

        // $response = $mailchimp->campaigns->list("d11f9e002a");

        return response()->json([$campaign]);
    }
}