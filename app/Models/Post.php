<?php

namespace App\Models;

use \DateTimeInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;


class Post extends Model 
{

    use SoftDeletes, HasSlug;

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }


    public $table = 'posts';

    public $filterable = [
        'id',
        'user.first_name',
        'title',
        'published',
    ];

    public $orderable = [
        'id',
        'user.first_name',
        'title',
        'published',
        'featured',
    ];


    protected $casts = [
        'featured' => 'boolean',
        'active'   => 'boolean',
        'id' => 'int',
    ];

    protected $dates = [
        'published',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'title',
        'summary',
        'content',
        'published',
        'featured',
        'active',
        'image',
        'readtime',
        'category_id'
    ];



    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->withTrashed();
    }

    public function setPublishedAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['published'] = Carbon::createFromFormat('d-m-Y H:i', $input)->format('Y-m-d H:i:00');
        } else {
            $this->attributes['published'] = null;
        }
    }

    public function getPublishedAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], 'd-m-Y H:i:s');

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $input)->format('d-m-Y H:i:s');
        } else {
            return '';
        }
    }


    // public function getPublishedAttribute($value)
    // {
    //     return $value ? Carbon::createFromFormat('Y-m-d H:i', $value)->format('d-m-Y H:i:00') : null;
    // }

    // public function setPublishedAttribute($value)
    // {
    //     $this->attributes['published'] = $value ? Carbon::createFromFormat('d-m-Y H:i:00', $value)->format('Y-m-d H:i') : null;
    // }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
