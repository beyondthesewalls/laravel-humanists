@props([
    'type' => 'text',
    'name' => '',
    'label' => '',
    'value' => '',
    'placeholder' => '',
])

<div>
    @if ($label != '')
    <label for='{{ $name }}' class="block text-sm font-medium text-gray-400">{{ $label }}</label>
    @endif
    <input type='{{ $type }}' name='{{ $name }}' id='{{ $name }}' value='{{ $slot }}' {!! $attributes->merge([
            'class' => 'transition duration-200 border border-gray-300 bg-white text-gray-900 appearance-none sm:text-md sm:leading-5 rounded-sm py-2 px-2 focus:border-custom-400 focus:outline-none mb-4' . ($label ? ' mt-1' : '')
        ]) !!}>
</div>