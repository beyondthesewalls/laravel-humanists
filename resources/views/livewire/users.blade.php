

<div>
    <h1 class="text-2xl font-medium text-gray-700">Users</h1>

    <div class="py-4 space-y-4">
        <!-- Top Bar -->
        <div class="flex justify-between">
            <div class="w-2/4 flex space-x-4 ">
           
                <x-input.search leading-icon="true" reset-filters="resetFilters()" wire:model="filters.search" placeholder="Search Users..." />

                <div class="radio-group inline-flex flex-1 flex-row flex-initial justify-center rounded-lg mt-2 rounded-md shadow-sm">
                    <div class="radio">
                        <label class="rounded-md rounded-r-none py-2 px-4 border text-sm leading-5 font-medium transition duration-150 ease-in-out {{ $role  == 0 ? 'bg-custom-600 text-white border-custom-600' : 'bg-gray-50 text-gray-500 border-gray-300'}}">
                            <input type="radio" name="role" id="option1" value="0" wire:model="role" > All
                        </label>
                    </div>
                    <div class="radio">
                        <label class="py-2 px-4 border text-sm leading-5 font-medium focus:outline-none transition duration-150 ease-in-out {{ $role  == 1 ? 'bg-custom-600 text-white border-custom-600' : 'bg-gray-50 text-cool-gray-500 border-gray-300'}}">
                            <input type="radio" name="role" id="option2" value="1" wire:model="role" >Admin
                        </label> 
                    </div>
                    <div class="radio">
                        <label class="rounded-md rounded-l-none py-2 px-4 border text-sm leading-5 font-medium transition duration-150 ease-in-out  {{ $role  == 2 ? 'bg-custom-600 text-white border-custom-600' : 'bg-gray-50 text-cool-gray-500 border-gray-300'}}">
                            <input type="radio" name="role" id="option3" value="2" wire:model="role" > User
                        </label>
                    </div>
                </div>
            </div>          

            <div class="space-x-2 flex items-end">
                <x-input.group borderless paddingless  for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage" class="w-20">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>
                <x-button.primary wire:click="create"><x-icon.plus/> New</x-button.primary>
            </div>
        </div>

             <!-- Transactions Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading sortable multi-column wire:click="sortBy('title')" :direction="$sorts['title'] ?? null" class="">Name</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('status')" :direction="$sorts['status'] ?? null"  class="w-60">Roles</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('status')" :direction="$sorts['status'] ?? null" class="w-10">Status</x-table.heading>
                    <x-table.heading class="w-10"></x-table.heading>
                </x-slot>

                <x-slot name="body">
                    @forelse ($users as $user)
                    <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $user->id }}">                        
                        <x-table.cell>
                            <span href="#" class="inline-flex space-x-2 truncate text-sm leading-5">
                               <p class="text-cool-gray-600 truncate">
                                    {{ $user->full_name }}
                                </p>
                            </span>
                        </x-table.cell>


                        <x-table.cell>
                            @foreach ($user->roles as $singleRole) 
                                <span class="rounded-lg py-1 px-2 mr-1 {!! $singleRole->color !!}">{!! $singleRole->title !!}</span>
                            @endforeach
                        </x-table.cell>

                        <x-table.cell>
                            @if ($user->status == 1 )
                                <div class="rounded-lg py-1 px-2 bg-green-50 justify-center">Active</div>
                            @else
                                <div class="rounded-lg py-1 px-2 bg-red-50 justify-center">Inactive</div>
                            @endif
                         </x-table.cell>

                        <x-table.cell class="flex justify-end">
                            <a wire:click="edit({{ $user->id }})">
                                <x-icon.edit class="h-8 w-8 text-green-600 flex"/> 
                            </a>
                            <x-button.link wire:click="delete({{ $user->id }})">
                                <x-icon.trash class="h-8 w-8 text-red-600 flex"/> 
                            </x-button.link>
                        </x-table.cell>
                    </x-table.row>
                    @empty
                    <x-table.row>
                        <x-table.cell colspan="6">
                            <div class="flex justify-center items-center space-x-2">
                                <x-icon.inbox class="h-8 w-8 text-cool-gray-400" />
                                <span class="font-medium py-8 text-cool-gray-400 text-xl">No users found...</span>
                            </div>
                        </x-table.cell>
                    </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>


            


            <div>
                {{ $users->links() }}
            </div>

        </div>
    </div>


    <!-- Delete Transactions Modal -->
    <form wire:submit.prevent="deleteSingle">
        <x-modal.confirmation wire:model.defer="showDeleteSingleModal">
            <x-slot name="title">Delete User</x-slot>

            <x-slot name="content">
                <div class="py-8 text-cool-gray-700">Are you sure?</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteSingleModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Delete</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>

    <!-- Save Transaction Modal -->
    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">Edit User</x-slot>

            <x-slot name="content">
                <x-input.group for="name" label="First Name" :error="$errors->first('editing.name')">
                    <x-input.text wire:model="editing.name" id="title" placeholder="First Name" />
                </x-input.group>

                <x-input.group for="last_name" label="Last Name" :error="$errors->first('editing.last_name')">
                    <x-input.text wire:model="editing.last_name" id="last_name" placeholder="Last Name" />
                </x-input.group>

                <x-input.group x-cloak for="email" label="Email" :error="$errors->first('editing.email')">
                    <x-input.text wire:model="editing.email" id="email" placeholder="Email" />
                </x-input.group>

                <x-input.group x-cloak for="roles" label="Roles" :error="$errors->first('roles')">
                    <x-input.multiple-select wire:model.defer="roles" :options="$myroles" />
                </x-input.group>

                
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>




     

    </form>

</div>


<script>



    </script>