@props(['options' => []])
<div wire:ignore>
<div x-data="tags({selected: @entangle($attributes->wire('model')), options: {{ $options }}})" class="relative bg-white overflow-y-visible z-10" @click.away="open = false"  >
  <div class="border border-gray-300 flex flex-wrap p-1 rounded-md text-sm" style="min-height: 50px">
    <template x-for="(item, index) in getSelected()" :key="index">
      <div class="flex border-2 text-white rounded-full py-1 px-2 m-1 bg-custom-500 border-custom-500">
        <div class="flex 32xl:items-end">
          <span x-text="item[labelField]"></span>
          <div class="focus:outline-none" x-on:click="removeItem(item[valueField])">
            <svg class="h-5 w-5 ml-1 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                 stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
            </svg>
          </div>
        </div>
      </div>
    </template>
    
  </div>
  <div class="mt-3">
    <x-button.secondary class="text-sm bg-gray-100" x-on:click="toggleOpen()">Select</x-button.secondary>

    <input type="button" name="Button" value="Select All" x-on:click="selectAll()" class="py-2 px-4 border rounded-md text-sm leading-5 font-medium focus:outline-none focus:border-custom-300 focus:shadow-outline-custom transition duration-150 ease-in-out text-white bg-custom-600 hover:bg-custom-500 active:bg-custom-700 border-custom-600" /> 
    <input type="button" name="Button" value="Deselect All" x-on:click="deselectAll()" class="py-2 px-4 border rounded-md text-sm leading-5 font-medium focus:outline-none focus:border-custom-300 focus:shadow-outline-custom transition duration-150 ease-in-out text-white bg-custom-600 hover:bg-custom-500 active:bg-custom-700 border-custom-600" /> 
    </div>

  <ul x-show="open" class="absolute w-full overflow-auto bg-white mt-1 p-1 border-2 border-gray-200 rounded-md space-y-1" style="height:190px">
    <template x-for="option in options" :key="option[valueField]">
      <li class="px-2 py-1 hover:bg-custom-500 hover:text-white cursor-pointer rounded-md "
          x-bind:class="isSelected(option[valueField]) ? 'bg-custom-200' : ''"
          x-on:click="toggleSelected(option[valueField])"
      >
        <span x-text="option[labelField]"></span>
      </li>
    </template>
  </ul>


</div>

</div>

@push('scripts')
  <script>
      function tags(config) {
          return {
              open: false,
              tag: '',
              selected: config.selected ?? [],
              options: config.options ?? [],
              valueField: 'id',
              labelField: 'title',
              toggleOpen() {
                  this.open = ! this.open;
              },
              toggleClose() {
                  this.open = false;
              },
              getSelected()
              {
                  return this.options.filter(option => {
                      return this.selected.includes(option[this.valueField]);
                  });
              },
              isSelected(value) {
                  return this.selected.includes(value);
              },
              toggleSelected(value) {
                  if (this.selected.includes(value)) {
                      this.selected = this.selected.filter(sel => {
                          return sel !== value;
                      })
                  } else {
                      this.selected = this.selected.concat([value]);
                  }
              },
              removeItem(value) {
                  this.selected = this.selected.filter(sel => {
                      return sel !== value;
                  })
              },
              selectAll() { 

                const array = this.options;
                var arr = [];
                array.forEach(function (item) {
                    arr.push(item.id);
                });
                  this.selected = this.selected.concat(arr);
              },

              deselectAll() { 

                this.selected = [];

              }
          }

      }
  </script>
@endpush