<div class="grid grid-cols-2 gap-6">
  <div>
    <label for="first_name" class="block text-sm font-medium text-gray-700">Page Title</label>
        {!! Form::text('title', old('title'), ['class' => 'px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150', '']) !!}
        @if($errors->has('title'))
                    <p class="help-block">
                       {{ $errors->first('title') }}
                    </p>
                @endif

                 <label for="last_name" class="block text-sm font-medium text-gray-700 mt-5">Menu Title</label>
        {!! Form::text('menu_title', old('menu_title'), ['class' => 'px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150', '']) !!}
        @if($errors->has('menu_title'))
            <p class="help-block">
               {{ $errors->first('menu_title') }}
            </p>
        @endif
    </div>
  <div>
    <label for="first_name" class="block text-sm font-medium text-gray-700">Order</label>
        {!! Form::text('order', old('order'), ['class' => 'px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150', '']) !!}
    
    <div class="grid grid-cols-2 gap-6 mt-5">
        <div>
            <label for="menu" class="inline-flex items-center mt-3">
                {!! Form::hidden('menu', 0) !!}
                 {!! Form::checkbox('menu', 1, old('menu', old('menu')), ['class' => 'form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out']) !!}
            
                <span class="ml-2 text-sm font-medium text-gray-700">Show on Menu</span>
            </label>

    </div>
        <div>

            <label for="active" class="inline-flex items-center mt-3">
                {!! Form::hidden('active', 0) !!}
                 {!! Form::checkbox('active', 1, old('active', old('active')), ['class' => 'form-checkbox h-5 w-5 text-indigo-600 transition duration-150 ease-in-out']) !!}
            
                <span class="ml-2 text-sm font-medium text-gray-700">Active</span>
            </label>


 
         <input type="hidden" name="content" id="content">
            
            </div>
        </div>
</div>
  </div>
</div>





