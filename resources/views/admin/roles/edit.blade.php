@extends('layouts.admin')


@section('content')

<h1 class="text-2xl font-semibold text-gray-900 flex">
    <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 mr-1 h-7 w-7 content-start" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z" />
    </svg> Role: Edit 
</h1>


<div class="min-w-full shadow sm:rounded-lg bg-white p-5 mt-5 ">

     @livewire('role.edit', [$role])

</div>

@endsection