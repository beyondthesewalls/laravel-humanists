

<div class="mt-4">
    <label class="block">
        @if ($label != '')
        <label for='{{ $name }}' class="block text-sm font-medium text-gray-400">{{ $label }}</label>
        @endif

        <select
                name="{{ $name }}"
    

            {!! $attributes->merge([
            'class' => 'transition duration-200 border border-gray-300 bg-white text-gray-900 appearance-none sm:text-md sm:leading-5 rounded-sm py-2 px-2 focus:border-custom-400 focus:outline-none mb-4' . ($label ? ' mt-1' : '')
        ]) !!}>
            @foreach($options as $key => $value)
                <option value="{{ $key }}" @if ($key == $setvalue) selected="selected" @endif>{{ $value }}</option>
            @endforeach
        </select>
    </label>

</div>