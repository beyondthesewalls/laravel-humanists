<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Page extends Model
{
    use SoftDeletes, HasSlug;

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    const STATUSES = [
        '0' => 'Hidden',
        '1' => 'Active',
    ];

    public $table = 'pages';

    public $orderable = [
        'id',
        'title',
        'order',
    ];

    public $filterable = [
        'id',
        'title',
        'order',
    ];

    protected $casts = [
        'menu'   => 'boolean',
        'active' => 'boolean',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'menu_title',
        'title',
        'content',
        'image_id',
        'menu',
        'order',
        'active',
    ];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getStatusColorAttribute()
    {
        return [
            '1' => 'green',
            '0' => 'red',
        ][$this->active] ?? 'cool-gray';
    }

    public function getDateForHumansAttribute()
    {
        return $this->date->format('M, d Y');
    }

    public function getDateForEditingAttribute()
    {
        return $this->date->format('m/d/Y');
    }

    public function setDateForEditingAttribute($value)
    {
        $this->date = Carbon::parse($value);
    }
}
